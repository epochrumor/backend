#!/bin/sh

set -e

MONGO_CONTAINER=backend_test_mongo
MONGO_PORT=27018
GANACHE_PORT=7545
GANACHE_CONTAINER=backend_test_ganache
TEST_PRIVATE_KEY="0x79fdeafa05035cea8540dc2355f5d85284346f09286f351ed4a908afd1659f62"

docker stop $MONGO_CONTAINER 2> /dev/null || true
docker stop $GANACHE_CONTAINER 2> /dev/null || true

docker run \
  -d \
  --rm \
  --name $GANACHE_CONTAINER \
  -p "$GANACHE_PORT:8545" \
  trufflesuite/ganache-cli:latest --account "$TEST_PRIVATE_KEY,0x56BC75E2D63100000"
# test account address: 0xF57aCC4e069703aD65C4f4eF410d4C50904f31C0
# contains 100 ether (0x56BC75E2D63100000 wei)

docker run \
  -d \
  --rm \
  --name $MONGO_CONTAINER \
  -p "$MONGO_PORT:27017" \
  mongo:4.2

WEB_TOKEN_SECRET=$(randomstring) \
  NODE_ENV="test" \
  DB_URI="mongodb://localhost:$MONGO_PORT" \
  MOCK_ACCOUNTS="true" \
  GETH_URL="http://127.0.0.1:$GANACHE_PORT" \
  TEST_PRIVATE_KEY=$TEST_PRIVATE_KEY \
  ENABLE_DAEMONS="true" \
  SUBSCRIPTION_MS="40000" \
  nyc --reporter=lcov --reporter=text --reporter=html ava --timeout 4m $@ || ERRORED=true true

echo ""
echo ""
echo "Keeping database and blockchain up, press return to continue"
echo ""
read

docker stop $MONGO_CONTAINER || true
docker stop $GANACHE_CONTAINER || true

if [ -z $ERRORED ]
then
  lcov-badge-generator ./coverage/lcov.info -o ./test/badge.svg
else
  exit 1
fi
