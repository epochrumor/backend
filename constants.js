module.exports = {
  // Users
  MIN_USERNAME_LENGTH: 4,
  MAX_USERNAME_LENGTH: 15,
  MIN_PASSWORD_LENGTH: 8,
  // Notes
  MIN_DESCRIPTION_LENGTH: 20,
  MAX_DESCRIPTION_LENGTH: 500,
  MIN_TEXT_LENGTH: 300,
  MAX_REACTION_LENGTH: 6,
}
