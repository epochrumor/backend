const jwt = require('jsonwebtoken')
const mongoose = require('mongoose')
const User = mongoose.model('User')

module.exports = {
  auth,
  authOptional,
  adminAuth,
}

function auth(req, res, next) {
  try {
    const token = req.body.token || req.query.token
    req.user = loadUser(token)
    next()
  } catch (err) {
    res.status(401).json({
      details: err.toString(),
      message: `You are not logged in`
    })
  }
}

function authOptional(req, res, next) {
  try {
    const token = req.body.token || req.query.token
    req.user = loadUser(token)
  } catch (err) {
    req.user = {}
  }
  next()
}

async function adminAuth(req, res, next) {
  try {
    const token = req.body.token || req.query.token
    req.user = loadUser(token)
    const adminCount = await User.countDocuments({
      _id: req.user._id,
      isAdmin: true
    }).exec()
    if (adminCount === 1) {
      next()
    } else {
      res.status(401).json({
        message: 'You are not admin'
      })
    }
  } catch (err) {
    res.status(401).json({
      details: err.toString(),
      message: 'You are not logged in'
    })
  }
}

function loadUser(token) {
  if (!token) throw new Error('No token supplied')
  const user = jwt.verify(token, process.env.WEB_TOKEN_SECRET)
  /* istanbul ignore else */
  if (user._id) {
    // Cast into ObjectId
    user._id = mongoose.Types.ObjectId(user._id)
  } else {
    throw new Error('No _id present on decoded user')
  }
  return user
}
