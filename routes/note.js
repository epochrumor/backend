const { auth, authOptional } = require('../middleware/auth')
const diff_match_patch = require('diff-match-patch')
const mongoose = require('mongoose')
const Note = mongoose.model('Note')
const NoteView = mongoose.model('NoteView')
const NotePatch = mongoose.model('NotePatch')
const NoteRead = mongoose.model('NoteRead')
const User = mongoose.model('User')
const Subscription = mongoose.model('Subscription')
const NoteReaction = mongoose.model('NoteReaction')
const {
  MIN_DESCRIPTION_LENGTH,
  MAX_DESCRIPTION_LENGTH,
  MIN_TEXT_LENGTH,
  MAX_REACTION_LENGTH,
} = require('../constants')
const {
  updateAll,
  updateNoteReadCount,
  updateReadsForNote,
  createPatch,
  loadNoteRevision,
} = require('../actions/note')
const _ = require('lodash')
const error = require('../middleware/catch_error')

module.exports = (app) => {
  app.post('/notes', auth, error(createNote))
  app.post('/get/notes', error(loadNotes))
  app.post('/get/notes/drafts', auth, error(loadDrafts))
  // Pass either a note id or a view id
  app.post('/delete/notes/:id', auth, error(deleteNote))
  app.post('/get/notes/:id', authOptional, error(loadNote))
  app.post('/put/notes/:id', auth, error(updateNote))
  app.post('/get/notes/:id/revisions', authOptional, error(loadNotePatches))
  app.post('/get/notes/:id/revision/:revindex', authOptional, error(loadNoteVersion))
  app.post('/notes/:id/reaction', auth, error(createReaction))
  app.post('/notes/:id/reaction/validate', error(verifyReaction))
  app.post('/notes/:id/related', error(loadRelatedNotes))
}

async function loadRelatedNotes(req, res) {
  const { id: _noteId } = req.params
  const noteId = mongoose.Types.ObjectId(_noteId)
  const note = await Note.findOne({
    _id: noteId,
  }).exec()
  if (!note) {
    res.status(404).json({
      message: `Unable to find note with id "${id}`,
    })
    return
  }
  const noteNumbers = []
  const noteRegex = /\s#[0-9]+(?![a-zA-Z])/g
  let result
  const fullText = `${note.description} ${note.text}`
  while ((result = noteRegex.exec(fullText)) !== null) {
    const [ match ] = result
    // cut the space character and pound sign
    const num = match.slice(2)
    noteNumbers.push(num)
  }
  const referencedNotes = _.chain(noteNumbers)
    .uniq()
    .map((n) => +n)
    .value()
  const notes = await Note.find({
    index: {
      $in: referencedNotes,
    }
  })
    .populate({
      path: 'owner',
      select: '-passwordHash'
    })
    .lean()
    .exec()
  res.json(notes)
}

async function verifyReaction(req, res) {
  const { id: _noteId } = req.params
  const noteId = mongoose.Types.ObjectId(_noteId)
  const { reaction } = req.body
  if (typeof reaction !== 'string' || reaction.length > MAX_REACTION_LENGTH) {
    res.json({
      reactionValid: false,
      existingCount: 0,
    })
    return
  }
  const existingCount = await NoteReaction.countDocuments({
    noteId,
    reaction,
  }).exec()
  res.json({
    reactionValid: true,
    existingCount,
  })
}

async function createReaction(req, res) {
  const { id: _noteId } = req.params
  const noteId = mongoose.Types.ObjectId(_noteId)
  const { reaction } = req.body
  if (typeof reaction !== 'string' || reaction.length > MAX_REACTION_LENGTH) {
    res.status(400).json({
      message: 'Reaction is invalid',
    })
    return
  }
  const note = await Note.findOne({
    _id: noteId,
    index: {
      $gte: 0,
    },
  }).exec()
  if (note === null) {
    res.status(404).json({
      message: 'Could not find note'
    })
    return
  }
  const existingReaction = await NoteReaction.findOne({
    ownerId: req.user._id,
    noteId,
  }).exec()
  if (existingReaction !== null) {
    await NoteReaction.updateOne({
      ownerId: req.user._id,
      noteId,
    }, {
      reaction,
      updatedAt: new Date(),
    })
  } else {
    await NoteReaction.create({
      ownerId: req.user._id,
      noteId,
      reaction,
      createdAt: new Date(),
      updatedAt: new Date(),
    })
  }
  await updateNoteReactions(noteId, [
    reaction,
    existingReaction && existingReaction.reaction,
  ])
  const _note = await Note.findOne({
    _id: noteId,
  }).select('-passwordHash').lean().exec()
  res.json(_note)
}

async function updateNoteReactions(noteId, _reactions = []) {
  const reactions = _.chain(_reactions)
    .compact()
    .uniq()
    .value()
  const reactionPromises = []
  for (const reaction of reactions) {
    reactionPromises.push((async () => {
      const count = await NoteReaction.countDocuments({
        noteId,
        reaction,
      }).exec()
      const { nModified } = await Note.updateOne({
        _id: noteId,
        'reactions.reaction': reaction,
      }, {
        $set: {
          'reactions.$': {
            reaction,
            count,
          }
        }
      })
      if (nModified === 0) {
        await Note.updateOne({
          _id: noteId,
        }, {
          $push: {
            reactions: {
              reaction,
              count,
            }
          }
        })
      }
    })())
  }
  await Promise.all(reactionPromises)
  await Note.updateOne({
    _id: noteId,
  }, {
    $pull: {
      'reactions': {
        count: 0,
      },
    },
  })
}

async function loadDrafts(req, res) {
  const notes = await Note.find({
    ownerId: req.user._id,
    isDraft: true,
    index: {
      $gte: 0,
    }
  }).sort({ updatedAt: 'desc' }).exec()
  res.json(notes)
}

async function loadNoteVersion(req, res) {
  const { id, revindex } = req.params
  const noteQuery = isNaN(+id) ? {
    _id: mongoose.Types.ObjectId(id),
  } : {
    index: +id,
  }
  const note = await Note.findOne(noteQuery)
    .select('_id')
    .exec()
  if (note === null) {
    res.status(404).json({
      message: 'Unable to find note'
    })
    return
  }
  try {
    const revision = await loadNoteRevision(note._id, revindex)
    res.json(revision)
  } catch (err) {
    res.status(400).json({
      message: err.toString(),
    })
  }
}

async function loadNotePatches(req, res) {
  const { id } = req.params
  let query
  if (isNaN(+id)) {
    query = {
      noteId: mongoose.Types.ObjectId(id),
    }
  } else {
    const note = await Note.findOne({
      index: +id,
    }).select('_id').exec()
    if (note === null) {
      res.status(404).json({
        message: 'Unable to find note'
      })
      return
    }
    query = {
      noteId: note._id
    }
  }
  const patches = await NotePatch.find(query)
    .sort({ index: 'desc'})
    .lean()
    .exec()
  if (patches.length === 0) {
    res.status(404).json({
      message: 'Unable to find note'
    })
    return
  }
  res.json(patches)
}

async function updateNote(req, res) {
  const { id } = req.params
  const _id = mongoose.Types.ObjectId(id)
  const { text, description, isDraft } = req.body
  if (!isDraft && (!text || text.length < MIN_TEXT_LENGTH)) {
    res.status(400).json({
      message: `Invalid text, must be ${MIN_TEXT_LENGTH} characters or more`
    })
    return
  }
  if (
    !isDraft &&
    (!description ||
    description.length > MAX_DESCRIPTION_LENGTH ||
    description.length < MIN_DESCRIPTION_LENGTH)
  ) {
    res.status(400).json({
      message: `Note description must be ${MIN_DESCRIPTION_LENGTH} - ${MAX_DESCRIPTION_LENGTH} characters`,
    })
    return
  }
  const subscribed = await Subscription.countDocuments({
    ownerId: req.user._id,
    active: true,
  }).exec()
  if (subscribed === 0 && !isDraft) {
    res.status(401).json({
      message: 'You are not subscribed!'
    })
    return
  }
  const note = await Note.findOne({
    _id,
  }).exec()
  if (note === null) {
    res.status(404).json({
      message: 'Unable to find note'
    })
    return
  } else if (note.ownerId.toString() !== req.user._id.toString()) {
    res.status(401).json({
      message: 'Not authorized to edit note',
    })
    return
  }
  if (typeof isDraft === 'boolean') {
    await Note.updateOne({
      _id: note._id,
      ownerId: req.user._id,
    }, {
      updatedAt: new Date(),
      isDraft,
    })
  }
  if (note.text === text && note.description === description) {
    // No patch necessary
    const _note = await Note.findOne({
      _id,
    }).populate('owner').lean().exec()
    res.json(_note)
    return
  }
  await createPatch(note._id, {
    description,
    text,
    ownerId: req.user._id,
    isDraft,
  })
  const updatedNote = await Note.findOne({
    _id,
  })
    .lean()
    .exec()
  res.json(updatedNote)
}

async function deleteNote(req, res) {
  const { id } = req.params
  const _id = mongoose.Types.ObjectId(req.params.id)
  const note = await Note.findOne({
    _id
  }).exec()
  if (!note) {
    res.status(404).json({
      message: `Unable to find note with id "${id}"`,
    })
    return
  }
  const { nModified } = await Note.updateOne({
    _id,
    ownerId: req.user._id
   }, {
    index: -1,
    text: '--deleted--'
  })
  if (nModified === 1) {
    res.status(204).end()
  } else {
    res.status(401).json({
      message: `Only the note owner can delete`
    })
  }
}

async function createNote(req, res) {
  const subscribed = await Subscription.countDocuments({
    ownerId: req.user._id,
    active: true,
  }).exec()
  const { text, description, isDraft, isUnmonetized } = req.body
  if (subscribed === 0 && !isDraft) {
    res.status(401).json({
      message: 'You are not subscribed!'
    })
    return
  }
  if (!isDraft && (!text || text.length < MIN_TEXT_LENGTH)) {
    res.status(400).json({
      message: `Note must be at least ${MIN_TEXT_LENGTH} characters`,
    })
    return
  }
  if (
    !isDraft &&
    (!description ||
    description.length > MAX_DESCRIPTION_LENGTH ||
    description.length < MIN_DESCRIPTION_LENGTH)
  ) {
    res.status(400).json({
      message: `Note description must be between ${MIN_DESCRIPTION_LENGTH} and ${MAX_DESCRIPTION_LENGTH} characters`,
    })
    return
  }
  const { _doc } = await Note.create({
    text,
    description,
    createdAt: new Date(),
    updatedAt: new Date(),
    index: -2, // return -2 for note creation failure
    ownerId: req.user._id,
    isDraft,
    isUnmonetized,
  })
  // Wait 1 second to account for variation in _id
  // https://docs.mongodb.com/manual/reference/method/ObjectId/#ObjectIDs-BSONObjectIDSpecification
  const p1 = (async () => {
    await new Promise(r => setTimeout(r, 1000))
    const index = await Note.countDocuments({
      _id: {
        $lt: _doc._id,
      },
    }).exec()
    await Note.updateOne({
      _id: _doc._id,
    }, {
      index,
    })
  })()
  await Promise.all([
    p1,
    createPatch(_doc._id, {
      ownerId: req.user._id,
      description,
      text,
    }),
  ])
  const note = await Note.findOne({
    _id: _doc._id,
  }).lean().exec()
  res.json(note)
}

async function loadNote(req, res) {
  const { id } = req.params
  const query = isNaN(+id) ? {
    _id: mongoose.Types.ObjectId(id),
  } : {
    index: +id,
  }
  const note = await Note.findOne(query)
    .populate({
      path: 'owner',
      select: '-passwordHash',
    })
    .lean()
    .exec()
  if (note === null) {
    res.status(404).json({
      message: `Unable to find note with id/index "${id}"`,
    })
    return
  }
  res.json(note)
}

async function userIdByUsername(username = '') {
  const user = await User.findOne({
    username: username.toLowerCase()
  }).select('_id').exec()
  if (user === null) return
  return user._id
}

async function loadNotes(req, res) {
  const id = req.body._id
  const usernames = req.body.usernames && JSON.parse(req.body.usernames)
  const _ids = []
  if (usernames && usernames.length) {
    const ids = await Promise.all(usernames.map(userIdByUsername))
    _ids.push(...ids)
  }
  const notes = await Note.find({
    index: {
      $gte: 0,
    },
    ...(_ids.length ? {
      ownerId: {
        $in: _ids
      },
    } : {}),
    isDraft: {
      $ne: true,
    },
  }).limit(50).sort({
    createdAt: 'desc'
  }).populate({
    path: 'owner',
    select: '-passwordHash'
  }).lean().exec()
  res.json(notes)
}
