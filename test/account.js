const test = require('ava')
const axios = require('axios')
const {
  noteBody,
  userBody,
  startServer,
  randomstring,
  unwrapAndThrow,
  deposit,
  createUser,
} = require('./helpers')
const { MIN_TEXT_LENGTH, MIN_DESCRIPTION_LENGTH } = require('../constants')
const randomObjectId = require('random-objectid')
const BN = require('bn.js')

let _shutdown
test.before(async (t) => {
  const { port, shutdown } = await startServer()
  _shutdown = shutdown
  axios.defaults.baseURL = `http://localhost:${port}`
})

test.after(async (t) => {
  await _shutdown()
})

test.beforeEach(async (t) => {
  const user = await createUser(axios)
  const { data: note } = await axios.post('/notes', {
    ...noteBody(),
    token: user.token,
  }).catch(unwrapAndThrow)
  t.context.note = note
  t.context.user = user
  // Wait 500 ms for ganache timestamp to advance
  await new Promise(r => setTimeout(r, 500))
})

test('should fail to load payout transactions if not admin', async (t) => {
  t.plan(1)
  const { user } = t.context
  await axios.post('/get/accounts/transactions', {
    token: user.token
  }).catch(unwrapAndThrow).catch((err) => t.is(err.status, 401))
})

test('should load payout transactions', async (t) => {
  const { data: admin } = await axios.post('/users/login', {
    username: 'admin',
    password: 'default_password',
  }).catch(unwrapAndThrow)
  await axios.post('/get/accounts/transactions', {
    token: admin.token,
  }).catch(unwrapAndThrow)
  t.pass()
})

test('should fail to load transactions without auth', async (t) => {
  t.plan(1)
  const { user } = t.context
  await axios.post('/get/transactions')
    .catch(unwrapAndThrow)
    .catch((err) => t.is(err.status, 401))
})

test('should load transactions', async (t) => {
  const { user } = t.context
  await new Promise(r => setTimeout(r, 10000))
  const { data } = await axios.post('/get/transactions', {
    token: user.token,
  }).catch(unwrapAndThrow)
  const { account, transactions } = data
  // User should have deposit and subscription
  t.is(transactions.length, 2)
})

test('should show deposit', async (t) => {
  const { user } = t.context
  const { data: account } = await axios.post('/get/account', {
    token: user.token,
  }).catch(unwrapAndThrow)
  // Send some wei
  const amount = '10000000'
  await deposit(account.address, amount)
  await new Promise(r => setTimeout(r, 10000))
  const { data: { transactions } } = await axios.post('/get/transactions', {
    token: user.token,
  }).catch(unwrapAndThrow)
  const tx = transactions.filter((_tx) => _tx.type === 'deposit').shift()
  if (tx.amount !== amount) {
    console.log(transactions)
  }
  t.is(tx.state, 2)
  t.is(tx.type, 'deposit')
  t.is(tx.amount, amount)
})

test('should create pending transactions', async (t) => {
  const { user, note } = t.context
  const { data: note2 } = await axios.post('/notes', {
    ...noteBody(),
    token: user.token,
  }).catch(unwrapAndThrow)
  const reader = await createUser(axios)
  const readTime = 80000
  const startTime = (+new Date()) - readTime
  const firstViews = []
  const secondViews = []
  for (let x = 0; x < readTime; x += 1000) {
    firstViews.push({
      noteId: note._id,
      createdAt: new Date(startTime + x),
      noteWidth: 100,
      noteHeight: 100,
      scrollOffset: 0,
      viewportWidth: 100,
      viewportHeight: 100,
    })
    secondViews.push({
      noteId: note2._id,
      createdAt: new Date(startTime + x),
      noteWidth: 100,
      noteHeight: 100,
      scrollOffset: 0,
      viewportWidth: 100,
      viewportHeight: 100,
    })
  }
  await axios.post('/noteviews', {
    views: firstViews,
    token: reader.token,
  }).catch(unwrapAndThrow)
  // Remove when async note read calculation is handled
  await new Promise(r => setTimeout(r, 1000))
  const { data: { transactions } } = await axios.post('/get/transactions', {
    token: user.token,
  }).catch(unwrapAndThrow)
  const [ tx ] = transactions
  t.is(tx.type, 'pending')
  t.is(tx.subscriptionOwnerId, reader._id)
  await axios.post('/noteviews', {
    views: secondViews,
    token: reader.token,
  }).catch(unwrapAndThrow)
  // Remove when async note read calculation is handled
  await new Promise(r => setTimeout(r, 1000))
  const { data } = await axios.post('/get/transactions', {
    token: user.token,
  }).catch(unwrapAndThrow)
  const [ tx2 ] = data.transactions
  t.is(tx2.type, 'pending')
  t.is(tx2.subscriptionOwnerId, reader._id)
})

test('should update account', async (t) => {
  const { user } = t.context
  const payoutAddress = '0x0000000000000000000000000000000000000000'
  const { data: account } = await axios.post('/get/account', {
    token: user.token,
  }).catch(unwrapAndThrow)
  await axios.post(`/put/accounts/${account._id}`, {
    autorenew: true,
    payoutAddress,
    token: user.token,
  }).catch(unwrapAndThrow)
  const { data: _account } = await axios.post('/get/account', {
    token: user.token,
  }).catch(unwrapAndThrow)
  t.is(_account.autorenew, true)
  t.is(_account.payoutAddress, payoutAddress)
  await axios.post(`/put/accounts/${account._id}`, {
    token: user.token,
  }).catch(unwrapAndThrow)
  const { data: __account } = await axios.post('/get/account', {
    token: user.token,
  }).catch(unwrapAndThrow)
  t.is(__account.autorenew, _account.autorenew)
  t.is(__account.payoutAddress, _account.payoutAddress)
})

test('should fail to update bad account', async (t) => {
  const { user } = t.context
  await axios.post(`/put/accounts/${randomObjectId()}`, {
    token: user.token,
  }).catch(unwrapAndThrow).catch((err) => t.is(err.status, 404))
})

test('should load subscription', async (t) => {
  const { user } = t.context
  const { data: subscription } = await axios.post('/get/subscription', {
    token: user.token
  }).catch(unwrapAndThrow)
  t.is(subscription.active, true)
})

test('should fail to load subscription', async (t) => {
  await axios.post('/get/subscription').catch(unwrapAndThrow).catch((err) => t.is(err.status, 401))
})

test('should fail to load inactive subscription', async (t) => {
  const { data: user } = await axios.post('/users', {
    ...userBody(),
  }).catch(unwrapAndThrow)
  await axios.post('/get/subscription', {
    token: user.token,
  }).catch(unwrapAndThrow).catch((err) => t.is(err.status, 404))
})

test('should fail to double subscribe', async (t) => {
  const { user } = t.context
  await axios.post('/put/subscription', {
    token: user.token,
  }).catch(unwrapAndThrow).catch((err) => t.is(err.status, 422))
})

test('should fail to subscribe with too little balance', async (t) => {
  const { data: user } = await axios.post('/users', {
    ...userBody(),
  }).catch(unwrapAndThrow)
  await new Promise(r => setTimeout(r, 1000))
  const { data: account } = await axios.post('/get/account', {
    token: user.token,
  }).catch(unwrapAndThrow)
  if (!account) {
    throw new Error('Account was not created')
  }
  const { data: subscriptionPrice } = await axios.post('/get/subscription/price', {
    token: user.token,
  }).catch(unwrapAndThrow)
  const amount = new BN(subscriptionPrice.price).sub(new BN(1))
  await deposit(account.address, amount)
  await axios.post('/put/subscription', {
    token: user.token,
  }).catch(unwrapAndThrow).catch((err) => t.is(err.status, 422))
})

test('should verify dividends', async (t) => {
  const { user, note } = t.context
  const reader = await createUser(axios)
  const readTime = 80000
  const startTime = (+new Date()) - readTime
  const firstViews = []
  const secondViews = []
  for (let x = 0; x < readTime; x += 1000) {
    firstViews.push({
      noteId: note._id,
      createdAt: new Date(startTime + x),
      noteWidth: 100,
      noteHeight: 100,
      scrollOffset: 0,
      viewportWidth: 100,
      viewportHeight: 100,
    })
  }
  await axios.post('/noteviews', {
    views: firstViews,
    token: reader.token,
  }).catch(unwrapAndThrow)
  await new Promise(r => setTimeout(r, +process.env.SUBSCRIPTION_MS + 10000))
  const { data: { transactions } } = await axios.post('/get/transactions', {
    token: user.token,
  }).catch(unwrapAndThrow)
  const [ tx ] = transactions
  if (tx.type !== 'dividend') {
    console.log(transactions)
  }
  t.is(tx.type, 'dividend')
})

test('should autorenew subscription', async (t) => {
  const { user, note } = t.context
  const reader = await createUser(axios)
  const readTime = 80000
  const startTime = (+new Date()) - readTime
  const firstViews = []
  const secondViews = []
  for (let x = 0; x < readTime; x += 1000) {
    firstViews.push({
      noteId: note._id,
      createdAt: new Date(startTime + x),
      noteWidth: 100,
      noteHeight: 100,
      scrollOffset: 0,
      viewportWidth: 100,
      viewportHeight: 100,
    })
  }
  await axios.post('/noteviews', {
    views: firstViews,
    token: reader.token,
  }).catch(unwrapAndThrow)
  // Reset timeout
  const { data: account } = await axios.post('/get/account', {
    token: user.token,
  }).catch(unwrapAndThrow)
  await axios.post(`/put/accounts/${account._id}`, {
    autorenew: true,
    token: user.token,
  }).catch(unwrapAndThrow)
  // Throw if no active subscription
  await axios.post('/get/subscription', {
    token: user.token
  }).catch(unwrapAndThrow)
  // send an ether for next subscription
  await deposit(account.address, '1000000000000000000')
  await new Promise(r => setTimeout(r, +process.env.SUBSCRIPTION_MS + 10000))
  const { data: subscription } = await axios.post('/get/subscription', {
    token: user.token
  }).catch(unwrapAndThrow)
  t.is(subscription.active, true)
})

test('should fail to autorenew subscription', async (t) => {
  const [ user, reader ] = await Promise.all([
    createUser(axios),
    createUser(axios),
  ])
  const { data: note } = await axios.post('/notes', {
    ...noteBody(),
    token: user.token,
  }).catch(unwrapAndThrow)
  const readTime = 80000
  const startTime = (+new Date()) - readTime
  const firstViews = []
  for (let x = 0; x < readTime; x += 1000) {
    firstViews.push({
      noteId: note._id,
      createdAt: new Date(startTime + x),
      noteWidth: 100,
      noteHeight: 100,
      scrollOffset: 0,
      viewportWidth: 100,
      viewportHeight: 100,
    })
  }
  await axios.post('/noteviews', {
    views: firstViews,
    token: reader.token,
  }).catch(unwrapAndThrow)
  // Reset timeout
  const { data: account } = await axios.post('/get/account', {
    token: user.token,
  }).catch(unwrapAndThrow)
  await axios.post(`/put/accounts/${account._id}`, {
    autorenew: true,
    token: user.token,
  }).catch(unwrapAndThrow)
  // Throw if no active subscription
  await axios.post('/get/subscription', {
    token: user.token
  }).catch(unwrapAndThrow)
  await new Promise(r => setTimeout(r, +process.env.SUBSCRIPTION_MS + 5000))
  await axios.post('/get/subscription', {
    token: user.token
  }).catch(unwrapAndThrow).catch(err => t.is(err.status, 404))
})
