module.exports = (fn) => {
  return async (req, res, next) => {
    try {
      await fn(req, res, next)
    } catch (err) {
      res.status(500).json({
        message: `Uncaught error: ${err.toString()}`,
        stack: err.stack,
      })
    }
  }
}
