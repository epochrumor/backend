const https = require('https')
const fs = require('fs')
const express = require('express')
const mongoose = require('mongoose')
const compression = require('compression')
const path = require('path')
const { shutdown, addShutdownListener } = require('./shutdown')

mongoose.set('useCreateIndex', true)
require('./models/user')
require('./models/note')
require('./models/note_view')
require('./models/follow')
require('./models/session')
require('./models/account')
require('./models/subscription')
require('./models/note_patch')
require('./models/note_read')
require('./models/account_transaction')
require('./models/request')
require('./models/note_reaction')
require('./models/block')

const app = express()

const loadingPromise = (async () => {
  try {
    // const sslKey = fs.readFileSync(path.join(__dirname, 'certs/k.pem'))
    // const sslCert = fs.readFileSync(path.join(__dirname, 'certs/cert1.pem'))
    // const sslCA = fs.readFileSync(path.join(__dirname, 'certs/ca.pem'))
    await mongoose.connect(process.env.DB_URI, {
      connectTimeoutMS: 5000,
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      // ssl: true,
      // sslKey,
      // sslCert,
      // sslCA,
    })
    await startServer()
    if (process.env.LISTENING_PORT) {
      const server = app.listen(process.env.LISTENING_PORT)
      addShutdownListener(async () => {
        await new Promise((rs) => {
          server.close(async () => {
            console.log('HTTP server closed')
            rs()
          })
        })
      })
    }
    await startDaemons()
    addShutdownListener(async () => {
      await mongoose.connection.close()
      console.log('Database connection closed')
    })
  } catch (err) {
    /* istanbul ignore next */
    {
      console.log('Error connecting to db')
      console.log(err)
      process.exit(1)
    }
  }
})()

function startServer() {
  app.use(express.json())
  app.use((_, res, next) => {
    res.set('Access-Control-Allow-Origin', '*')
    res.set('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE, PUT')
    res.set('Access-Control-Allow-Headers', 'Origin, Content-Type, Access-Control-Allow-Origin')
    next()
  })
  app.use(compression({ threshold: 0 }))

  // require('./middleware/timing')(app)

  require('./routes/user')(app)
  require('./routes/note')(app)
  require('./routes/follow')(app)
  require('./routes/note_view')(app)
  require('./routes/account')(app)
}

async function startDaemons() {
  /* istanbul ignore else */
  if (process.env.ENABLE_DAEMONS) {
    require('./daemons/dividends')
    // require('./daemons/rebuild_reads')
    require('./daemons/payout')
    require('./daemons/chain')
  }

  /* istanbul ignore else */
  if (process.env.MOCK_ACCOUNTS) {
    require('./daemons/accounts_mock')
  }
  // Create a default admin account in the test env
  /* istanbul ignore else */
  if (process.env.NODE_ENV === 'test') {
    const { bootstrap } = require('./actions/bootstrap')
    await bootstrap()
  }
}

module.exports = Object.assign(app, {
  shutdown,
  loadingPromise,
})
