const mongoose = require('mongoose')

const FollowSchema = new mongoose.Schema(
  {
    ownerId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    createdAt: {
      type: Date,
      required: true,
    },
    authorId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    }
  }
)

FollowSchema.index({
  ownerId: 1,
  authorId: 1,
}, {
  unique: true,
})

mongoose.model('Follow', FollowSchema)
