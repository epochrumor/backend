const mongoose = require('mongoose')

const SessionSchema = new mongoose.Schema({
  ownerId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  userAgent: {
    type: String,
  },
  platform: {
    type: String,
  },
  cookieEnabled: {
    type: String,
  },
  language: {
    type: String,
  },
  ip: {
    type: String,
  },
  createdAt: {
    type: Date,
    required: true,
  },
  tokenHash: {
    type: String,
    required: true,
  },
})

mongoose.model('Session', SessionSchema)
