const mongoose = require('mongoose')

const AccountTransactionSchema = new mongoose.Schema(
  {
    accountId: {
      type: mongoose.Schema.Types.ObjectId,
      required: false,
    },
    ownerId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    createdAt: {
      type: Date,
      required: true,
    },
    state: {
      // 0 = unconfirmed, 1 = broadcasted, 2 = confirmed
      type: Number,
      required: true,
      default: 0,
    },
    blockNumber: {
      type: Number,
      required: true,
    },
    blockTimestamp: {
      type: Date,
      required: true,
    },
    nonce: {
      type: Number,
    },
    transactionId: {
      type: String,
      required: true,
      index: {
        unique: true,
      },
    },
    gas: {
      type: String,
      required: false,
    },
    gasPrice: {
      type: String,
      required: false,
    },
    type: {
      type: String,
      required: true,
    },
    // estimate for pending transactions
    amount: {
      type: String,
      required: true,
    },
    maturesAt: {
      type: Date,
    },
    subscriptionId: {
      type: mongoose.Schema.Types.ObjectId,
    },
    subscriptionOwnerId: {
      type: mongoose.Schema.Types.ObjectId,
    },
    newBalance: {
      type: String,
    },
    from: {
      type: String,
    },
    to: {
      type: String,
    },
    // Only used in testing
    testLock: {
      type: Boolean,
    },
  }
)

AccountTransactionSchema.virtual('subscriptionOwner', {
  ref: 'User',
  localField: 'subscriptionOwnerId',
  foreignField: '_id',
  justOne: true,
})

AccountTransactionSchema.virtual('account', {
  ref: 'Account',
  localField: 'accountId',
  foreignField: '_id',
  justOne: true,
})

AccountTransactionSchema.virtual('sender', {
  ref: 'Account',
  localField: 'from',
  foreignField: 'address',
  justOne: true,
})

AccountTransactionSchema.index({
  transactionId: 1,
  state: 1,
})

AccountTransactionSchema.index({
  ownerId: 1,
  accountId: 1,
  type: 1,
})

AccountTransactionSchema.index({
  from: 1,
  ownerId: 1,
  type: 1,
  state: 1,
})

mongoose.model('AccountTransaction', AccountTransactionSchema)
