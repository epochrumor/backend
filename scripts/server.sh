#!/bin/sh

set -e

NAME=epochrumor_backend

docker stop $NAME || true
docker rm $NAME || true

docker run -d \
  --restart=always \
  --name $NAME \
  -p 4000:4000 \
  -v ${PWD}/.env:/src/.env \
  $(docker build . -q)
