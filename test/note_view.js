const test = require('ava')
const axios = require('axios')
const {
  noteBody,
  userBody,
  startServer,
  randomstring,
  unwrapAndThrow,
  deposit,
  createUser,
} = require('./helpers')
const { MIN_TEXT_LENGTH, MIN_DESCRIPTION_LENGTH } = require('../constants')
const randomObjectId = require('random-objectid')

let _shutdown
test.before(async (t) => {
  const { port, shutdown } = await startServer()
  _shutdown = shutdown
  axios.defaults.baseURL = `http://localhost:${port}`
})

test.after(async (t) => {
  await _shutdown()
})

test.beforeEach(async (t) => {
  const user = await createUser(axios)
  const { data: note } = await axios.post('/notes', {
    ...noteBody(),
    token: user.token,
  }).catch(unwrapAndThrow)
  t.context.note = note
})

// noteid verification

test('should fail to create view', async (t) => {
  t.plan(3)
  const { user, note } = t.context
  // TODO: better error handling for schema validation failures
  // Test bad object id
  const { data: views } = await axios.post('/noteviews', {
    views: [
      {
        noteId: randomObjectId(),
        createdAt: new Date(),
        noteWidth: 100,
        noteHeight: 100,
      }
    ]
  }).catch(unwrapAndThrow)
  t.assert(views.createdCount === 0)
  // Test future date
  await axios.post('/noteviews', {
    views: [
      {
        noteId: note._id,
        createdAt: +new Date() + 5000,
        noteWidth: 100,
        noteHeight: 100,
      }
    ]
  }).catch(unwrapAndThrow).catch((err) => t.is(err.status, 400))
  // Test without date
  await axios.post('/noteviews', {
    views: [
      {
        noteId: note._id,
      }
    ]
  }).catch(unwrapAndThrow).catch((err) => t.pass())
})

test('should create view without auth', async (t) => {
  const { user, note } = t.context
  const { data: views } = await axios.post('/noteviews', {
    views: [
      {
        noteId: note._id,
        createdAt: new Date(),
        noteWidth: 100,
        noteHeight: 100,
      }
    ]
  }).catch(unwrapAndThrow)
  t.is(views.createdCount, 1)
})
