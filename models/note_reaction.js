const mongoose = require('mongoose')

const NoteReactionSchema = new mongoose.Schema(
  {
    ownerId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    noteId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    reaction: {
      type: String,
      required: true,
    },
    createdAt: {
      type: Date,
      required: true,
    },
    updatedAt: {
      type: Date,
      required: true,
    }
  }
)

mongoose.model('NoteReaction', NoteReactionSchema)
