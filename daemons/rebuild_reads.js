const mongoose = require('mongoose')
const User = mongoose.model('User')
const Account = mongoose.model('Account')
const Subscription = mongoose.model('Subscription')
const AccountTransaction = mongoose.model('AccountTransaction')
const NoteRead = mongoose.model('NoteRead')
const Note = mongoose.model('Note')
const Web3 = require('web3')
const BN = require('bn.js')
const { updateReadsForNote } = require('../actions/note')
const _ = require('lodash')
const uuid = require('uuid')

// Start a run loop
;(async () => {
  try {
    const notes = await Note.find({
      index: {
        $gte: 0,
      }
    }).exec()
    const users = await User.find({}).exec()
    let i = 0
    for (const note of notes) {
      for (const user of users) {
        if (i++ % 5 === 0) console.log(`${i}/${notes.length * users.length}...`)
        await updateReadsForNote(note._id, user._id)
      }
      await updateReadsForNote(note._id)
    }
  } catch (err) {
    console.log(`read rebuild daemon error`)
    console.log(err)
    process.exit(1)
  }
})()
