const Web3 = require('web3')
const mongoose = require('mongoose')
const Account = mongoose.model('Account')
const AccountTransaction = mongoose.model('AccountTransaction')
const Block = mongoose.model('Block')
const BN = require('bn.js')
const {
  updateBalance,
  updateBalanceDelta
} = require('../actions/account')
const {
  addShutdownListener,
} = require('../shutdown')
const {
  acquireLock,
  releaseLock
} = require('../actions/account_lock')

const {
  CHAIN_BLOCK_START,
} = process.env
const startBlock = CHAIN_BLOCK_START && +CHAIN_BLOCK_START || 0

let shuttingDown = false
let shutdownFinished
function shutdown() {
  return new Promise((rs) => {
    shutdownFinished = rs
    shuttingDown = true
  })
}
addShutdownListener(shutdown)

;(async () => {
  for (;;) {
    try {
      const web3 = new Web3(process.env.GETH_URL)
      const [ bestBlock, lastBlock ] = await Promise.all([
        web3.eth.getBlockNumber(),
        // Get the latest processed block
        Block.findOne({
          processed: true,
          number: {
            $gte: startBlock,
          },
        }).sort({ number: 'desc' }).exec()
      ])
      let nextBlock = startBlock
      let lastBlockNumber = 0
      if (lastBlock) {
        nextBlock = lastBlock.number + 1
        lastBlockNumber = lastBlock.number
      }
      const promises = []
      while (nextBlock <= bestBlock && nextBlock - lastBlockNumber < 50) {
        promises.push(processBlock(nextBlock++))
      }
      await Promise.all(promises)
      if (!promises.length){
        await new Promise(r => setTimeout(r, 2500))
      }
    } catch (err) {
      console.log('Error syncing chain', err)
    } finally {
      // Check for shutdown
      if (shuttingDown) {
        shutdownFinished()
        break
      }
    }
  }
})()

async function processBlock(blockNumber) {
  try {
    const existing = await Block.findOne({
      number: blockNumber,
    }).exec()
    const now = +new Date()
    if (existing && existing.processed) {
      return
    } else if (
      existing &&
      !existing.processed &&
      now - +existing.createdAt < 10000
    ) {
      // existing block was create less than 10 seconds ago
      // give it time to be processed by another thread
      return
    } else if (!existing) {
      // try to create
      await Block.create({
        number: blockNumber,
        createdAt: new Date(),
      })
    }
  } catch (err) {
    if (err.code === 11000) {
      // Another process is processing this block
      return
    }
    throw err
  }
  const web3 = new Web3(process.env.GETH_URL)
  // Web3 or geth seems to have a bug with extremely recent blocks
  let block = await web3.eth.getBlock(blockNumber, true)
  while (block === null) {
    console.log(new Date().toISOString(), 'Block load failed, retrying')
    await new Promise(r => setTimeout(r, 500))
    block = await web3.eth.getBlock(blockNumber, true)
  }
  // Process each transaction in parallel
  const txCount = await Account.countDocuments({
    address: {
      $in: block.transactions.map(({ from, to }) => [from, to]).flat()
    }
  }).exec()
  if (txCount === 0) {
    await Block.updateOne({
      number: blockNumber,
    }, {
      processed: true,
    })
    return
  }
  const promises = []
  for (const tx of block.transactions) {
    promises.push(processBlockTx(tx, block))
  }
  await Promise.all(promises)
  await Block.updateOne({
    number: blockNumber,
  }, {
    processed: true,
  })
}

async function processBlockTx(tx, block) {
  const existing = await AccountTransaction.countDocuments({
    transactionId: tx.hash,
    state: 2,
  }).exec()
  if (existing === 1) {
    return
  }
  const [
    sendingAccount,
    receivingAccount,
  ] = await Promise.all([
    Account.findOne({
      address: tx.from,
    }).exec(),
    Account.findOne({
      address: tx.to,
    }).exec(),
  ])
  if (sendingAccount) {
    const gasAmount = (new BN(tx.gasPrice)).mul(new BN(tx.gas))
    const { nModified } = await AccountTransaction.updateOne({
      state: 0,
      amount: tx.value,
      type: {
        $in: ['payout', 'refund'],
      },
      from: tx.from,
      to: tx.to,
    }, {
      transactionId: tx.hash,
      state: 1,
      blockNumber: block.number,
      blockTimestamp: new Date(block.timestamp * 1000),
      gasAmount,
    })
    if (nModified !== 1) {
      throw new Error('Unexpected transaction')
    }
    // Need a lock for the balance delta calculation
    const _locked = await acquireLock(sendingAccount._id, true, Infinity)
    const { nModified: _nModified } = await AccountTransaction.updateOne({
      transactionId: tx.hash,
    }, {
      state: 2,
    })
    await Promise.all([
      updateBalance(sendingAccount.ownerId),
      updateBalanceDelta(sendingAccount.ownerId),
    ])
    await releaseLock(sendingAccount._id)
    if (_nModified !== 1) {
      throw new Error(`Unable to mark tx "${tx.hash}" complete`)
    }
  } else if (receivingAccount) {
    await Promise.all([
      updateBalance(receivingAccount.ownerId),
      AccountTransaction.create({
        ownerId: receivingAccount.ownerId,
        accountId: receivingAccount._id,
        createdAt: new Date(),
        blockNumber: block.number,
        blockTimestamp: new Date(block.timestamp * 1000),
        state: 2,
        transactionId: tx.hash,
        amount: tx.value,
        type: 'deposit',
        from: tx.from,
        to: tx.to,
      }),
    ])
  }
}
