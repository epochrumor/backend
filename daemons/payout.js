const Web3 = require('web3')
const BN = require('bn.js')
const mongoose = require('mongoose')
const Account = mongoose.model('Account')
const AccountTransaction = mongoose.model('AccountTransaction')
const User = mongoose.model('User')
const {
  updateBalanceDelta,
  loadEthPrice,
  updateBalance,
  toWei,
  toEther,
} = require('../actions/account')
const uuid = require('uuid')
const { acquireLock, releaseLock } = require('../actions/account_lock')
const { addShutdownListener } = require('../shutdown')

let shuttingDown = false
let shutdownFinished
function shutdown() {
  return new Promise((rs) => {
    shutdownFinished = rs
    shuttingDown = true
  })
}
addShutdownListener(shutdown)

;(async () => {
  for (;;) {
    try {
      await createTransactions()
    } catch (err) {
      /* istanbul ignore next */
      {
        console.log('Error creating transactions')
        console.log(err)
      }
    } finally {
      if (shuttingDown) {
        shutdownFinished()
        break
      }
      await new Promise(r => setTimeout(r, 2500))
    }
  }
})()

// Look for balances larger than project
async function createTransactions() {
  const price = await loadEthPrice()
  // Estimated subscription cost, at 150% (account for eth price fluctuation)
  const ethPrice = 1.5 * (4 / price)
  const weiPrice = new BN(toWei(ethPrice.toString()))
  // console.log(`Wei price: ${weiPrice.toString()}`)
  // Find accounts needing payout
  const accounts = await Account.find({
    payoutAddress: {
      $exists: true,
    },
  }).sort({
    balanceEther: 'desc',
  }).select(['_id', 'ownerId']).exec()
  for (const account of accounts) {
    await settleAccount(account._id, account.ownerId)
  }
}

async function settleAccount(accountId, ownerId) {
  const locked = await acquireLock(accountId)
  if (!locked) {
    // Another process has the lock
    return
  }
  // First check for pending payouts
  const pendingCount = await AccountTransaction.countDocuments({
    state: {
      $ne: 2,
    },
    type: {
      $in: ['payout', 'refund'],
    },
    accountId,
  }).exec()
  if (pendingCount !== 0) {
    await releaseLock(accountId)
    return
  }
  // If no pending payouts recalculate balances
  await Promise.all([
    updateBalance(ownerId),
    updateBalanceDelta(ownerId)
  ])
  // Then load the account
  const account = await Account.findOne({
    _id: accountId,
  }).exec()

  // Calculate some values needed later
  // TODO: Relate to gas price
  const minPayout = new BN(toWei('0.001'))
  const web3 = new Web3(process.env.GETH_URL)
  const gasPrice = new BN(await web3.eth.getGasPrice())
  const price = await loadEthPrice()
  const ethPrice = 1.5 * (4 / price)
  const weiPrice = new BN(toWei(ethPrice.toString()))
  const balance = new BN(account.balance)
  const balanceDelta = new BN(account.balanceDelta)
  // This is the positive balance excluding earned funds
  const availableBalance = balance.add(BN.min(balanceDelta, new BN(0)))
  const _gas = new BN(await web3.eth.estimateGas({
    from: account.address,
    to: account.payoutAddress,
  }))
  const _gasWei = _gas.mul(gasPrice)
  if (
    balanceDelta.lte(new BN(0)) &&
    availableBalance.sub(_gasWei).sub(weiPrice).gte(minPayout)
  ) {
    await AccountTransaction.create({
      accountId: account._id,
      ownerId: account.ownerId,
      createdAt: new Date(),
      state: 0,
      blockNumber: 0,
      blockTimestamp: new Date(),
      transactionId: uuid.v4(),
      type: 'refund',
      amount: availableBalance.sub(_gasWei).sub(weiPrice).toString(),
      from: account.address,
      to: account.payoutAddress,
      gas: _gas.toString(),
      gasPrice: gasPrice.toString(),
    })
    await releaseLock(accountId)
    return
  } else {
    // positive balance
    const weiKeep = BN.min(balanceDelta.sub(weiPrice), new BN(0))
    const amount = availableBalance.sub(_gasWei).add(weiKeep)
    if (amount.gte(minPayout)) {
      await AccountTransaction.create({
        accountId: account._id,
        ownerId: account.ownerId,
        createdAt: new Date(),
        state: 0,
        blockNumber: 0,
        blockTimestamp: new Date(),
        transactionId: uuid.v4(),
        type: 'refund',
        amount: amount.toString(),
        from: account.address,
        to: account.payoutAddress,
        gas: _gas.toString(),
        gasPrice: gasPrice.toString(),
      })
      await releaseLock(accountId)
      return
    }
  }

  // Payout if needed
  // Leave enough for a subscription
  let owedWei = balance.add(balanceDelta).sub(weiPrice)
  if (owedWei.lt(minPayout)) {
    await releaseLock(accountId)
    return
  }
  // Load potential payout sources
  const accounts = await Account.find({
    _id: {
      $ne: account._id,
    },
    balanceDeltaEther: {
      $lt: 0,
    },
    // Need gas to send
    balanceEther: {
      $gt: 0,
    },
  }).sort({ balanceDeltaEther: 'asc' }).exec()
  for (const withdrawalAccount of accounts) {
    const _locked = await acquireLock(withdrawalAccount._id)
    if (!_locked) {
      continue
    }
    const pendingTxCount = await AccountTransaction.countDocuments({
      from: withdrawalAccount.address,
      type: {
        $in: ['payout', 'refund'],
      },
      state: {
        $ne: 2,
      },
    }).exec()
    if (pendingTxCount !== 0) {
      await releaseLock(withdrawalAccount._id)
      continue
    }
    // Update the balance before running a withdraw
    await Promise.all([
      updateBalance(withdrawalAccount.ownerId),
      updateBalanceDelta(withdrawalAccount.ownerId),
    ])
    const acc = await Account.findOne({
      _id: withdrawalAccount._id,
    }).exec()
    const wBalance = new BN(acc.balance)
    const wBalanceDelta = new BN(acc.balanceDelta)
    const availableWei = wBalanceDelta.neg()
    if (availableWei.gt(new BN(0)) && availableWei.gt(wBalance)) {
      console.log(acc)
      throw new Error('Platform wei is less than balance')
    }
    const gas = new BN(await web3.eth.estimateGas({
      from: withdrawalAccount.address,
      to: account.payoutAddress,
    }))
    const gasWei = gas.mul(gasPrice)
    const wei = BN.min(owedWei, availableWei.sub(gasWei))
    if (wei.lt(minPayout)) {
      await releaseLock(withdrawalAccount._id)
      continue
    }
    await AccountTransaction.create({
      accountId: account._id,
      ownerId: account.ownerId,
      createdAt: new Date(),
      state: 0,
      blockNumber: 0,
      blockTimestamp: new Date(),
      transactionId: uuid.v4(),
      type: 'payout',
      amount: wei.toString(),
      from: acc.address,
      to: account.payoutAddress,
      gas: gas.toString(),
      gasPrice: gasPrice.toString(),
    })
    await releaseLock(withdrawalAccount._id)
    await releaseLock(accountId)
    return
  }
  await releaseLock(accountId)
}
