const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const mongoose = require('mongoose')
const { auth } = require('../middleware/auth')
const User = mongoose.model('User')
const Session = mongoose.model('Session')
const Subscription = mongoose.model('Subscription')
const sha1 = require('sha1')
const {
  MIN_USERNAME_LENGTH,
  MAX_USERNAME_LENGTH,
  MIN_PASSWORD_LENGTH,
} = require('../constants')
const error = require('../middleware/catch_error')

module.exports = (app) => {
  app.post('/users', error(createUser))
  app.post('/users/login', error(login))
  app.post('/get/users/authed', auth, error(authedUser))
  app.post('/get/users/:id', error(loadUser))
  app.post('/put/users/token/refresh', auth, error(refresh))
  app.post('/put/users/edit', auth, error(editUser))
  app.post('/users/validate', error(validateUser))
}

async function isUserSubscribed(userId) {
  const count = await Subscription.countDocuments({
    createdAt: {
      $lt: new Date(),
    },
    expiresAt: {
      $gt: new Date(),
    },
    active: true,
    ownerId: userId,
  }).exec()
  return count === 1
}

async function isUsernameValid(_username = '', _id = '') {
  const username = _username.toLowerCase()
  if (username.length < MIN_USERNAME_LENGTH) return false
  if (username.length > MAX_USERNAME_LENGTH) return false
  const user = await User.findOne({
    username,
  }).select('_id').exec()
  if (user && user._id.toString() === _id.toString()) return true
  if (user !== null) return false
  return true
}

async function isEmailValid(email = '') {
  return (email.indexOf('@') >= 1 && email.indexOf('.') !== -1)
}

async function isPasswordValid(password = '') {
  return password.length >= MIN_PASSWORD_LENGTH
}

// Checks if supplied field values are acceptable
async function validateUser(req, res) {
  const { username, email, password } = req.body
  const [ usernameValid, emailValid, passwordValid ] = await Promise.all([
    isUsernameValid(username),
    isEmailValid(email),
    isPasswordValid(password),
  ])
  res.json({
    username: usernameValid,
    password: passwordValid,
    email: emailValid,
  })
}

async function editUser(req, res) {
  const { username: _username, bio } = req.body
  const username = (_username || '').toLowerCase()
  const usernameValid = username
    ? (await isUsernameValid(username, req.user._id))
    : true
  if (!usernameValid) {
    res.status(422).json({
      message: 'This username is not valid'
    })
    return
  }
  const { nModified, ok } = await User.updateOne({
    _id: req.user._id,
  }, {
    ...(username ? {
      username,
    } : {}),
    ...(bio ? {
      bio,
    } : {}),
    updatedAt: new Date(),
  }).exec()
  if (nModified !== 1) {
    res.status(404).json({
      message: 'Failed to update document',
    })
    return
  }
  if (ok !== 1) {
    res.status(500).json({
      message: 'Unknown error occured'
    })
    return
  }
  const user = await User.findOne({
    _id: req.user._id,
  }).select('-passwordHash').lean().exec()
  res.json(user)
}

async function loadUser(req, res) {
  const { id } = req.params
  const username = id
  let _id = false
  try {
    _id = mongoose.Types.ObjectId(id)
  } catch (err) { /* use username */ }
  const user = await User.findOne({
    ...(_id ? {
      _id
    } : {}),
    ...(!_id ? {
      username,
    } : {}),
  }).lean().exec()
  if (user === null) {
    res.status(404).json({
      message: `Unable to find user with id ${id}`
    })
    return
  }
  res.json({
    ...user,
    passwordHash: '',
  })
}

async function authedUser(req, res) {
  const [ user, subscribed ] = await Promise.all([
    User.findOne({
      _id: req.user._id
    })
      .lean()
      .exec(),
    isUserSubscribed(req.user._id)
  ])
  const tenDays = 10 * 24 * 60 * 60 * 1000
  res.json({
    ...user,
    passwordHash: '',
    needsRefresh: (+new Date() - req.user.generatedAt) > tenDays,
    activeSubscription: subscribed,
    // needsRefresh: true,
  })
}

async function refresh(req, res) {
  const user = await User.findOne({
    _id: req.user._id
  })
    .lean()
    .exec()
  const token = jwt.sign({
    ...user,
    passwordHash: '',
    generatedAt: +new Date(),
  }, process.env.WEB_TOKEN_SECRET)
  await Session.create({
    ownerId: mongoose.Types.ObjectId(user._id),
    ip: req.headers['x-forwarded-for'],
    createdAt: new Date(),
    tokenHash: sha1(token),
    ...req.body,
  })
  res.json({
    ...user,
    token,
    passwordHash: '',
    needsRefresh: false,
  })
}

async function createUser(req, res) {
  if (!req.body.username) {
    res.status(400).json({
      message: 'No username supplied'
    })
    return
  }
  if (!req.body.email) {
    res.status(400).json({
      message: 'No email supplied'
    })
    return
  }
  // Take the whitespace off the username before using it
  const username = req.body.username.trim().toLowerCase()
  const email = req.body.email.trim().toLowerCase()
  const password = req.body.password
  if (username.length < MIN_USERNAME_LENGTH) {
    res.status(400).json({
      message: `Username should be at least${MIN_USERNAME_LENGTH} characters`
    })
    return
  }
  if (username.length > MAX_USERNAME_LENGTH) {
    res.status(400).json({
      message: `Username should be at most ${MAX_USERNAME_LENGTH} characters`
    })
    return
  }
  const [ usernameValid, emailValid, passwordValid ] = await Promise.all([
    isUsernameValid(username),
    isEmailValid(email),
    isPasswordValid(password),
  ])
  if (!usernameValid) {
    res.status(400).json({
      message: 'Username is taken'
    })
    return
  }
  if (!passwordValid) {
    res.status(400).json({
      message: `Password should be at least ${MIN_PASSWORD_LENGTH} characters`
    })
    return
  }
  if (!emailValid) {
    res.status(400).json({
      message: 'Email is in use or not valid'
    })
    return
  }
  const salt = await bcrypt.genSalt(10)
  const passwordHash = await bcrypt.hash(req.body.password, salt)
  // Create the user with the password hash
  // The plaintext password will automatically be excluded
  const { _doc } = await User.create({
    email,
    username,
    passwordHash,
    createdAt: new Date(),
    updatedAt: new Date(),
  })
  // Generate a token without the password hash
  const token = jwt.sign({
    ..._doc,
    passwordHash: '',
    generatedAt: +new Date(),
  }, process.env.WEB_TOKEN_SECRET)
  await Session.create({
    ownerId: mongoose.Types.ObjectId(_doc._id),
    ip: req.headers['x-forwarded-for'],
    createdAt: new Date(),
    tokenHash: sha1(token),
    ...req.body,
  })
  res.json({
    ..._doc,
    passwordHash: '',
    token,
  })
}

async function login(req, res) {
  const username = req.body.username.trim().toLowerCase()
  const user = await User.findOne({
    username,
  })
    .lean()
    .exec()
  if (!user) {
    res.status(404).json({
      message: 'This username is not registered'
    })
    return
  }
  const passwordMatch = await bcrypt.compare(
    req.body.password,
    user.passwordHash
  )
  if (!passwordMatch) {
    res.status(401).json({
      message: 'There was a problem logging you in',
    })
    return
  }
  const token = jwt.sign({
    ...user,
    passwordHash: '',
    generatedAt: +new Date(),
  }, process.env.WEB_TOKEN_SECRET)
  await Session.create({
    ownerId: mongoose.Types.ObjectId(user._id),
    ip: req.headers['x-forwarded-for'],
    createdAt: new Date(),
    tokenHash: sha1(token),
    ...req.body,
  })
  res.json({
    ...user,
    passwordHash: '',
    token,
  })
}
