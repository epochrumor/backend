const { authOptional, auth } = require('../middleware/auth')
const mongoose = require('mongoose')
const Note = mongoose.model('Note')
const Follow = mongoose.model('Follow')
const User = mongoose.model('User')
const _ = require('lodash')
const error = require('../middleware/catch_error')

module.exports = (app) => {
  app.post('/put/follow/:id', auth, error(createFollow))
  app.post('/delete/follow/:id', auth, error(deleteFollow))
  app.post('/get/follows', auth, error(getFollows))
}

async function getFollows(req, res) {
  const [ following, followers ] = await Promise.all([
    Follow.find({ ownerId: req.user._id }).exec(),
    Follow.find({ authorId: req.user._id }).exec()
  ])
  res.json({
    following: _.map(following, 'authorId'),
    followers: _.map(followers, 'ownerId'),
  })
}

async function deleteFollow(req, res) {
  const { id } = req.params
  const _id = mongoose.Types.ObjectId(id)
  const { ok, deletedCount } = await Follow.deleteOne({
    ownerId: req.user._id,
    authorId: _id,
  })
  if (deletedCount === 0) {
    res.status(404).json({
      message: `Couldn't find follow`
    })
    return
  }
  if (ok !== 1) {
    res.status(500).json({
      message: 'Unknown error deleting follow'
    })
    return
  }
  await Promise.all([
    updateFollowCounts(_id),
    updateFollowCounts(req.user._id),
  ])
  res.status(204).end()
}

async function createFollow(req, res) {
  const { id } = req.params
  const _id = mongoose.Types.ObjectId(id)
  const { _doc } = await Follow.create({
    createdAt: new Date(),
    ownerId: req.user._id,
    authorId: _id,
  })
  await Promise.all([
    updateFollowCounts(_id),
    updateFollowCounts(req.user._id),
  ])
  res.json(_doc)
}

async function updateFollowCounts(_userId) {
  const userId = mongoose.Types.ObjectId(_userId)
  const [ followingCount, followerCount ] = await Promise.all([
    Follow.countDocuments({ ownerId: userId }),
    Follow.countDocuments({ authorId: userId })
  ])
  await User.updateOne({
    _id: userId,
  }, {
    followingCount,
    followerCount,
  })
}

// I may not get it either but that won't stop me from acting like i get it
