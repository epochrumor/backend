const Web3 = require('web3')
const mongoose = require('mongoose')
const crypto = require('crypto')
const Account = mongoose.model('Account')
const AccountTransaction = mongoose.model('AccountTransaction')
const User = mongoose.model('User')
const secp256k1 = require('secp256k1')
const { addShutdownListener } = require('../shutdown')

let shuttingDown = false
let shutdownFinished
function shutdown() {
  return new Promise((rs) => {
    shutdownFinished = rs
    shuttingDown = true
  })
}
addShutdownListener(shutdown)

;(async () => {
  for (;;) {
    try {
      await createAccounts()
    } catch (err) {
      /* istanbul ignore next */
      {
        console.log('Error creating accounts')
        console.log(err)
      }
    } finally {
      if (shuttingDown) {
        shutdownFinished()
        break
      }
      await new Promise(r => setTimeout(r, 500))
    }
  }
})()

;(async () => {
  for (;;) {
    try {
      await createTransactions()
      await new Promise(r => setTimeout(r, 2500))
    } catch (err) {
      /* istanbul ignore next */
      {
        console.log('Error creating accounts')
        console.log(err)
      }
    }
  }
})()

/**
 * Idempotently creates accounts for users
 **/
async function createAccounts() {
  // Find users for which no Account document exists
  const users = await User.aggregate([
    {
      $lookup: {
        from: 'accounts',
        localField: '_id',
        foreignField: 'ownerId',
        as: 'accounts',
      },
    },
    { $match: { accounts: { $size: 0 }, }},
    { $project: { _id: 1, } },
  ]).exec()
  await Promise.all(users.map(async (user) => {
    // Create account with unencrypted private key
    // Do not use in production
    const { _id: ownerId } = user
    const wallet = getWallet()
    try {
      await Account.create({
        ownerId,
        createdAt: new Date(),
        address: wallet.address,
        publicKey: wallet.publicKey,
        privateKeyEncrypted: wallet.privateKey,
        updatedAt: new Date(),
      })
    } catch (err) {
      // ignore duplicate key errors
      /* istanbul ignore else */
      if (err.code !== 11000) {
        throw err
      }
    }
  }))
}

// Creates a new wallet
function getWallet() {
  const evenPad = key => key.length % 2 === 0 ? key : `0${key}`
  const toBytes = (hex, base = 16) =>
    new Uint8Array(evenPad(hex).match(/.{1,2}/g).map(byte => parseInt(byte, base)))
  const fromBytes = (bytes, base = 16) =>
  Array.prototype.map.call(bytes, (byte) => {
    return `0${byte.toString(base)}`.slice(-2)
  }).join('')
  const web3 = new Web3()
  const entropy = crypto.randomBytes(64)
  const { address, privateKey } = web3.eth.accounts.create(entropy)
  const publicKey = secp256k1.publicKeyCreate(toBytes(privateKey.replace('0x', '')))
  return {
    publicKey: fromBytes(publicKey),
    address,
    privateKey,
  }
}

async function createTransactions() {
  const transactions = await AccountTransaction.find({
    type: {
      $in: ['payout', 'refund']
    },
    state: 0,
  })
    .sort({ createdAt: 1 })
    .populate('sender')
    .lean()
    .exec()
  const txPromises = []
  for (const tx of transactions) {
    // Broadcast the transactions and update state
    txPromises.push(broadcastTransaction(tx, tx.sender.privateKeyEncrypted))
  }
  await Promise.all(txPromises)
}

/**
 * Take the decrypted private key and broadcast a transaction and wait for block receipts
 */
async function broadcastTransaction(transaction, privateKeyDecrypted) {
  const web3 = new Web3(process.env.GETH_URL)
  const { nModified } = await AccountTransaction.updateOne({
    _id: transaction._id,
    testLock: {
      $exists: false,
    },
  }, {
    testLock: true,
  }).exec()
  if (nModified !== 1) {
    return
  }
  const releaseLock = () => AccountTransaction.updateOne({
    _id: transaction._id,
  }, {
    testLock: false,
  }).exec()
  // console.log(`broadcasting ${transaction.transactionId}`, transaction)
  const { rawTransaction } = await web3.eth.accounts.signTransaction({
    from: transaction.from,
    to: transaction.to,
    value: transaction.amount,
    gas: transaction.gas,
    gasPrice: transaction.gasPrice,
  }, privateKeyDecrypted)
  await new Promise((rs, rj) => {
    web3.eth.sendSignedTransaction(rawTransaction)
      .on('confirmation', () => {
        rs()
      })
  })
  await releaseLock()
}
