const mongoose = require('mongoose')

const NoteReadSchema = new mongoose.Schema(
  {
    noteId: {
      type: mongoose.Types.ObjectId,
      required: true,
    },
    ownerId: {
      type: mongoose.Types.ObjectId,
      required: true,
    },
    viewIds: [mongoose.Types.ObjectId],
    createdAt: {
      type: Date,
      required: true,
    },
    updatedAt: {
      type: Date,
      required: true,
    },
    readPercent: {
      type: Number,
      required: true,
      default: 0,
    },
    readCount: {
      type: Number,
      required: true,
      default: 0,
    },
    readDates: [Date],
    lastReadAt: {
      type: Date,
    },
    totalReadSeconds: {
      type: Number,
      required: true,
      default: 0,
    },
    paidReadCount: {
      type: Number,
      required: true,
      default: 0,
    }
  }
)

mongoose.model('NoteRead', NoteReadSchema)
