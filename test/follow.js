const test = require('ava')
const axios = require('axios')
const { userBody, startServer, randomstring, unwrapAndThrow } = require('./helpers')

let _shutdown
test.before(async (t) => {
  const { port, shutdown } = await startServer()
  _shutdown = shutdown
  axios.defaults.baseURL = `http://localhost:${port}`
})

test.after(async (t) => {
  await _shutdown()
})

test.beforeEach(async (t) => {
  const { data: user } = await axios.post('/users', {
    ...userBody(),
  }).catch(unwrapAndThrow)
  t.context.user = user
})

test('should fail to follow', async (t) => {
  const { user } = t.context
  await axios.post(`/put/follow/${user._id}`)
    .catch(unwrapAndThrow).catch((err) => t.is(err.status, 401))
})

test('should follow a user', async (t) => {
  const { user } = t.context
  const { data: user2 } = await axios.post('/users', {
    ...userBody(),
  }).catch(unwrapAndThrow)
  await axios.post(`/put/follow/${user._id}`, {
    token: user2.token,
  }).catch(unwrapAndThrow)
  const { data: _user } = await axios.post(`/get/users/${user._id}`).catch(unwrapAndThrow)
  const { data: _user2 } = await axios.post(`/get/users/${user2._id}`).catch(unwrapAndThrow)
  t.assert(_user.followingCount === 0)
  t.assert(_user.followerCount === 1)
  t.assert(_user2.followingCount === 1)
  t.assert(_user2.followerCount === 0)
})

test('should fail to get follows if not authed', async (t) => {
  await axios.post('/get/follows')
    .catch(unwrapAndThrow).catch((err) => t.is(err.status, 401))
})

test('should get follows', async (t) => {
  const { user } = t.context
  const { data: user2 } = await axios.post('/users', {
    ...userBody(),
  }).catch(unwrapAndThrow)
  await axios.post(`/put/follow/${user._id}`, {
    token: user2.token,
  }).catch(unwrapAndThrow)
  const { data: follows } = await axios.post('/get/follows', {
    token: user.token,
  }).catch(unwrapAndThrow)
  t.assert(follows.followers.length === 1)
})

test('should fail to delete follow', async (t) => {
  const { user } = t.context
  const { data: user2 } = await axios.post('/users', {
    ...userBody(),
  }).catch(unwrapAndThrow)
  await axios.post(`/put/follow/${user._id}`, {
    token: user2.token,
  }).catch(unwrapAndThrow)
  await axios.post(`/delete/follow/${user2._id}`, {
    token: user.token,
  }).catch(unwrapAndThrow).catch((err) => t.is(err.status, 404))
})

test('should delete follow', async (t) => {
  const { user } = t.context
  const { data: user2 } = await axios.post('/users', {
    ...userBody(),
  }).catch(unwrapAndThrow)
  await axios.post(`/put/follow/${user._id}`, {
    token: user2.token,
  }).catch(unwrapAndThrow)
  const { data: _user } = await axios.post(`/get/users/${user._id}`).catch(unwrapAndThrow)
  const { data: _user2 } = await axios.post(`/get/users/${user2._id}`).catch(unwrapAndThrow)
  t.assert(_user.followerCount === 1)
  t.assert(_user2.followingCount === 1)
  await axios.post(`/delete/follow/${user._id}`, {
    token: user2.token,
  }).catch(unwrapAndThrow)
  const { data: __user } = await axios.post(`/get/users/${user._id}`).catch(unwrapAndThrow)
  const { data: __user2 } = await axios.post(`/get/users/${user2._id}`).catch(unwrapAndThrow)
  t.assert(__user.followerCount === 0)
  t.assert(__user2.followingCount === 0)
})
