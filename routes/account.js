const axios = require('axios')
const Web3 = require('web3')
const BN = require('bn.js')
const _ = require('lodash')
const mongoose = require('mongoose')
const {
  auth,
  adminAuth,
} = require('../middleware/auth')
const uuid = require('uuid')
const {
  findOrCreateSubscriptionOffer,
  updateBalanceDelta,
  updateBalance,
  createPendingTransactions,
  createTransactionsForBlock,
} = require('../actions/account')
const Account = mongoose.model('Account')
const AccountTransaction = mongoose.model('AccountTransaction')
const User = mongoose.model('User')
const Subscription = mongoose.model('Subscription')
const error = require('../middleware/catch_error')

module.exports = (app) => {
  app.post('/get/account', auth, error(loadAccount))
  app.post('/put/accounts/:id', auth, error(updateAccount))
  app.post('/put/subscription', auth, error(subscribe))
  app.post('/get/subscription', auth, error(loadSubscription))
  app.post('/get/subscription/price', auth, error(loadSubscriptionPrice))
  app.post('/get/transactions', auth, error(loadTransactions))
  app.post('/get/accounts/needed', adminAuth, error(loadNewUsers))
  // For settler use
  app.post('/accounts/create', adminAuth, error(createAccounts))
  app.post('/get/accounts/transactions', adminAuth, error(loadPayoutTransactions))
}

async function loadPayoutTransactions(req, res) {
  const transactions = await AccountTransaction.find({
    type: {
      $in: ['payout', 'refund'],
    },
    state: 0,
  })
    .sort({ createdAt: 1 })
    .populate('sender')
    .lean()
    .exec()
  // console.log(transactions)
  res.json(transactions)
}

async function createAccounts(req, res) {
  const { accounts } = req.body
  const docs = _.map(accounts, (account) => ({
    createdAt: new Date(),
    updatedAt: new Date(),
    ...account,
  }))
  await Account.insertMany(docs)
  res.status(204).end()
}

async function loadNewUsers(req, res) {
  // Calculate users without corresponding account
  const users = await User.aggregate([
    {
      $lookup: {
        from: 'accounts',
        localField: '_id',
        foreignField: 'ownerId',
        as: 'accounts',
      },
    },
    { $match: { accounts: { $size: 0 }, }},
    { $project: { _id: 1, } },
  ]).exec()
  res.json(_.map(users, '_id'))
}

async function updateAccount(req, res) {
  const { id } = req.params
  const _id = mongoose.Types.ObjectId(id)
  const account = await Account.findOne({
    _id,
    ownerId: req.user._id,
  }).select('-privateKeyEncrypted').exec()
  if (account === null) {
    res.status(404).json({
      message: 'Account not found',
    })
    return
  }
  const payoutAddress = req.body.payoutAddress || account.payoutAddress
  const autorenew = typeof req.body.autorenew === 'boolean' ? req.body.autorenew : account.autorenew
  await Account.updateOne({
    _id,
    ownerId: req.user._id,
  }, {
    autorenew,
    payoutAddress,
  })
  res.status(204).end()
}

async function subscribe(req, res) {
  const existingSubscription = await Subscription.findOne({
    createdAt: {
      $lt: new Date(),
    },
    expiresAt: {
      $gt: new Date(),
    },
    active: true,
    ownerId: req.user._id,
  }).exec()
  if (existingSubscription !== null) {
    res.status(422).json({
      message: 'User already has a valid subscription',
    })
    return
  }
  const [ subscription ] = await Promise.all([
    findOrCreateSubscriptionOffer(req.user._id),
    updateBalanceDelta(req.user._id),
    updateBalance(req.user._id),
  ])
  const account = await Account.findOne({
    ownerId: req.user._id,
  }).select('-privateKeyEncrypted').exec()
  if (!account) {
    res.status(400).json({
      message: 'No account exists, please wait to deposit funds'
    })
    return
  }
  const balance = new BN(account.balance)
  const delta = new BN(account.balanceDelta)
  const available = balance.add(delta)
  const subscriptionPrice = new BN(subscription.price)
  if (available.lt(subscriptionPrice)){
    res.status(422).json({
      message: 'Account balance is too low, deposit more Ether'
    })
    return
  }
  const { _doc: accountTransaction } = await AccountTransaction.create({
    accountId: account._id,
    ownerId: req.user._id,
    createdAt: new Date(),
    state: 0,
    blockNumber: 0,
    blockTimestamp: +new Date(), // placeholder for sorting
    type: 'payment',
    amount: subscriptionPrice.toString(),
    transactionId: uuid.v4(),
    from: account.address,
  })
  await Promise.all([
    updateBalanceDelta(req.user._id),
    Subscription.updateOne({
      _id: subscription._id,
    }, {
      active: true,
      accountTransactionId: accountTransaction._id,
    })
  ])
  const _subscription = await Subscription.findOne({
    _id: subscription._id,
  }).lean().exec()
  res.json(_subscription)
}

async function loadSubscriptionPrice(req, res) {
  const subscription = await findOrCreateSubscriptionOffer(req.user._id)
  res.json(Object.assign(subscription, {
    etherPrice: toEther(subscription.price),
  }))
}

function toEther(value) {
  const web3 = new Web3(process.env.GETH_URL)
  const etherValue = web3.utils.fromWei(new BN(value))
  return parseFloat(etherValue).toFixed(5)
}

async function loadTransactions(req, res) {
  await Promise.all([
    createPendingTransactions(req.user._id),
    updateBalance(req.user._id),
    updateBalanceDelta(req.user._id),
  ])
  const [ transactions, account ] = await Promise.all([
    AccountTransaction.find({
      ownerId: req.user._id,
    }).sort({ blockTimestamp: 'desc' }).populate('subscriptionOwner', 'username').lean().exec(),
    Account.findOne({
      ownerId: req.user._id,
    }).select('-privateKeyEncrypted').lean().exec(),
  ])
  if (!account) {
    res.status(404).json({
      message: 'Account does not exist yet'
    })
    return
  }
  res.json({
    account: {
      ...account,
      etherBalance: toEther(account.balance),
      etherAvailableBalance: toEther(new BN(account.balance).add(new BN(account.balanceDelta))),
    },
    transactions: transactions.map((tx) => ({
      ...tx,
      etherAmount: toEther(tx.amount),
      etherNewBalance: toEther(tx.newBalance),
    })),
  })
}

async function loadAccount(req, res) {
  const count = await Account.countDocuments({
    ownerId: req.user._id,
  }).exec()
  if (count === 0) {
    res.status(404).json({
      message: 'Account does not exist yet'
    })
    return
  }
  await Promise.all([
    updateBalance(req.user._id),
    updateBalanceDelta(req.user._id),
  ])
  const account = await Account.findOne({
    ownerId: req.user._id,
  }).select('-privateKeyEncrypted').lean().exec()
  res.json({
    ...account,
    etherBalance: toEther(account.balance),
    etherAvailableBalance: toEther(new BN(account.balance).add(new BN(account.balanceDelta))),
  })
}

async function loadSubscription(req, res) {
  const subscription = await Subscription.findOne({
    createdAt: {
      $lt: new Date(),
    },
    expiresAt: {
      $gt: new Date(),
    },
    active: true,
    ownerId: req.user._id,
  }).exec()
  if (!subscription) {
    res.status(404).json({
      message: 'No active subscription',
    })
    return
  }
  res.json(subscription)
}
