const mongoose = require('mongoose')

const RequestSchema = new mongoose.Schema(
  {
    ownerId: {
      type: mongoose.Schema.Types.ObjectId,
      required: false,
    },
    createdAt: {
      type: Date,
      required: true,
    },
    completedAt: {
      type: Date,
      required: false,
    },
    ip: {
      type: String,
      required: false,
    },
    path: {
      type: String,
      required: true,
    },
    method: {
      type: String,
      required: true,
    },
    auth: {
      type: String,
      required: false,
    },
    executionTimeMs: {
      type: Number,
      required: false,
    },
  }
)

mongoose.model('Request', RequestSchema)
