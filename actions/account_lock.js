const mongoose = require('mongoose')
const Account = mongoose.model('Account')

module.exports = {
  acquireLock,
  releaseLock,
}

async function acquireLock(accountId, wait = false, maxCount = 5) {
  let locked = await _acquireLock(accountId)
  let count = 0
  while (!locked && wait && count < maxCount) {
    // console.log('waiting', locked, wait, count, accountId)
    await new Promise(r => setTimeout(r, 500))
    count++
    locked = await _acquireLock(accountId)
  }
  return locked
}

async function _acquireLock(accountId) {
  const { nModified } = await Account.updateOne({
    _id: accountId,
    $or: [
      {
        _locked: false,
      }, {
        _locked: {
          $exists: false,
        }
      }
    ]
  }, {
    _locked: true
  }).exec()
  if (nModified !== 1) {
    // Another process has the lock
    return false
  }
  return true
}

async function releaseLock(accountId) {
  const { nModified } = await Account.updateOne({
    _id: accountId,
    _locked: true,
  }, {
    _locked: false,
  }).exec()
  if (nModified !== 1) {
    throw new Error(`Expected account "${accountId.toString()}" to be locked!`)
  }
}
