const mongoose = require('mongoose')

const NoteViewSchema = new mongoose.Schema({
  noteId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  ownerId: {
    type: mongoose.Schema.Types.ObjectId,
  },
  createdAt: {
    type: Date,
    required: true,
  },
  viewportWidth: {
    type: Number,
  },
  viewportHeight: {
    type: Number,
  },
  /**
   * screen width and height are unused as of now, consider removal
   **/
  screenWidth: {
    type: Number,
  },
  screenHeight: {
    type: Number,
  },
  clientWidth: {
    type: Number,
  },
  clientHeight: {
    type: Number,
  },
  noteWidth: {
    type: Number,
    required: true,
  },
  noteHeight: {
    type: Number,
    required: true,
  },
  scrollOffset: {
    type: Number,
  },
})

mongoose.model('NoteView', NoteViewSchema)
