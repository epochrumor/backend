const test = require('ava')
const axios = require('axios')
const Web3 = require('web3')
const {
  MIN_TEXT_LENGTH,
  MIN_DESCRIPTION_LENGTH,
  MAX_DESCRIPTION_LENGTH,
  MAX_REACTION_LENGTH,
} = require('../constants')
const {
  noteBody,
  userBody,
  startServer,
  randomstring,
  unwrapAndThrow,
  deposit,
  createUser,
} = require('./helpers')
const randomObjectId = require('random-objectid')

let _shutdown
test.before(async (t) => {
  const { port, shutdown } = await startServer()
  _shutdown = shutdown
  axios.defaults.baseURL = `http://localhost:${port}`
})

test.after(async (t) => {
  await _shutdown()
})

test.beforeEach(async (t) => {
  const user = await createUser(axios)
  const { data: note } = await axios.post('/notes', {
    ...noteBody(),
    token: user.token,
  }).catch(unwrapAndThrow)
  t.context.user = user
  t.context.note = note
})

test('should fail to create note without auth', async (t) => {
  await axios.post('/notes', {
    ...noteBody(),
  }).catch(unwrapAndThrow).catch((err) => t.is(err.status, 401))
})

test('should fail to create note without subscription', async (t) => {
  const { data: user } = await axios.post('/users', {
    ...userBody(),
  }).catch(unwrapAndThrow)
  await axios.post('/notes', {
    ...noteBody(),
    token: user.token,
  }).catch(unwrapAndThrow).catch((err) => t.is(err.status, 401))
})

test('should fail to create bad note', async (t) => {
  t.plan(3)
  const { user } = t.context
  const { token } = user
  // Text too short
  await axios.post('/notes', {
    text: randomstring(MIN_TEXT_LENGTH - 1),
    description: randomstring(MIN_DESCRIPTION_LENGTH),
    token,
  }).catch(unwrapAndThrow).catch((err) => t.is(err.status, 400))
  // Description too short
  await axios.post('/notes', {
    description: randomstring(MIN_DESCRIPTION_LENGTH - 1),
    text: randomstring(MIN_TEXT_LENGTH),
    token,
  }).catch(unwrapAndThrow).catch((err) => t.is(err.status, 400))
  // Description too long
  await axios.post('/notes', {
    description: randomstring(MAX_DESCRIPTION_LENGTH + 1),
    text: randomstring(MIN_TEXT_LENGTH),
    token,
  }).catch(unwrapAndThrow).catch((err) => t.is(err.status, 400))
})

test('should create note', async (t) => {
  const { user } = t.context
  const text = randomstring(MIN_TEXT_LENGTH)
  const description = randomstring(MIN_DESCRIPTION_LENGTH)
  const { data: note } = await axios.post('/notes', {
    text,
    description,
    token: user.token,
  }).catch(unwrapAndThrow)
  t.is(text, note.text)
  t.is(description, note.description)
  t.assert(note.createdAt)
  t.assert(note.updatedAt)
  t.is(note.ownerId, user._id)
  t.assert(note._id)
})

test('should fail to load note', async (t) => {
  t.plan(2)
  const { user, note } = t.context
  await axios.post(`/get/notes/${randomObjectId()}`)
    .catch(unwrapAndThrow).catch(err => t.is(err.status, 404))
  await axios.post(`/get/notes/${576187651879423}`)
    .catch(unwrapAndThrow).catch(err => t.is(err.status, 404))
})

test('should fail to update note with bad data', async (t) => {
  t.plan(3)
  const { user, note } = t.context
  const text = randomstring(MIN_TEXT_LENGTH)
  const description = randomstring(MIN_DESCRIPTION_LENGTH)
  await axios.post(`/put/notes/${note._id}`, {
    text: randomstring(MIN_TEXT_LENGTH - 1),
    description,
    token: user.token,
  }).catch(unwrapAndThrow).catch((err) => t.is(err.status, 400))
  await axios.post(`/put/notes/${note._id}`, {
    text,
    description: randomstring(MIN_DESCRIPTION_LENGTH - 1),
    token: user.token,
  }).catch(unwrapAndThrow).catch((err) => t.is(err.status, 400))
  await axios.post(`/put/notes/${note._id}`, {
    text,
    description: randomstring(MAX_DESCRIPTION_LENGTH + 1),
    token: user.token,
  }).catch(unwrapAndThrow).catch((err) => t.is(err.status, 400))
})

test('should fail to update note if not authed', async (t) => {
  t.plan(2)
  const { user, note } = t.context
  await axios.post(`/put/notes/${note._id}`, {
    text: randomstring(MIN_TEXT_LENGTH),
    description: randomstring(MIN_DESCRIPTION_LENGTH),
  }).catch(unwrapAndThrow).catch((err) => t.is(err.status, 401))
  const { data: otherUser } = await axios.post('/users', {
    ...userBody(),
  }).catch(unwrapAndThrow)
  await axios.post(`/put/notes/${note._id}`, {
    text: randomstring(MIN_TEXT_LENGTH),
    description: randomstring(MIN_DESCRIPTION_LENGTH),
    token: otherUser.token,
  }).catch(unwrapAndThrow).catch((err) => t.is(err.status, 401))
})

test('should fail to update note', async (t) => {
  const { user, note } = t.context
  await axios.post(`/put/notes/${randomObjectId()}`, {
    text: randomstring(MIN_TEXT_LENGTH),
    description: randomstring(MIN_DESCRIPTION_LENGTH),
    token: user.token,
  }).catch(unwrapAndThrow).catch(err => t.is(err.status, 404))
})

test('should update note with no change', async (t) => {
  const { user, note } = t.context
  await axios.post(`/put/notes/${note._id}`, {
    text: note.text,
    description: note.description,
    token: user.token,
  }).catch(unwrapAndThrow)
  const { data: newNote } = await axios.post(`/get/notes/${note._id}`).catch(unwrapAndThrow)
  t.is(newNote.text, note.text)
  t.is(newNote.description, note.description)
})

test('should update note', async (t) => {
  const { user, note } = t.context
  const { text, description } = noteBody()
  await axios.post(`/put/notes/${note._id}`, {
    text,
    description,
    token: user.token,
  }).catch(unwrapAndThrow)
  const { data: newNote1 } = await axios.post(`/get/notes/${note._id}`).catch(unwrapAndThrow)
  if (newNote1.description !== description) {
    const { data: notes } = await axios.post('/get/notes', {
      token: user.token,
    }).catch(unwrapAndThrow)
    console.log(notes)
    console.log({ description, text }, note, newNote1)
  }
  t.is(newNote1.isDraft, undefined)
  t.is(newNote1.description, description)
  t.is(newNote1.text, text)
  t.assert(newNote1.createdAt === note.createdAt)
  t.assert(newNote1.updatedAt !== note.updatedAt)
  await axios.post(`/put/notes/${note._id}`, {
    description: newNote1.description,
    text: newNote1.text,
    isDraft: true,
    token: user.token,
  }).catch(unwrapAndThrow)
  const { data: newNote2 } = await axios.post(`/get/notes/${note._id}`).catch(unwrapAndThrow)
  t.is(newNote2.isDraft, true)
  t.is(newNote2.createdAt, note.createdAt)
  await axios.post(`/put/notes/${note._id}`, {
    description: newNote1.description,
    text: newNote1.text,
    isDraft: false,
    token: user.token,
  }).catch(unwrapAndThrow)
  const { data: newNote3 } = await axios.post(`/get/notes/${note._id}`).catch(unwrapAndThrow)
  t.is(newNote3.isDraft, false)
  t.is(newNote3.createdAt, note.createdAt)
})

test('should fail to load note revision', async (t) => {
  t.plan(3)
  const { user, note } = t.context
  await axios.post(`/get/notes/${randomObjectId()}/revision/0`)
    .catch(unwrapAndThrow).catch(err => t.is(err.status, 404))
  await axios.post(`/get/notes/${note._id}/revision/48284824884284`)
    .catch(unwrapAndThrow).catch(err => t.is(err.status, 400))
  await axios.post(`/get/notes/${randomObjectId()}/revision/48284824884284`)
    .catch(unwrapAndThrow).catch(err => t.is(err.status, 404))
})

test('should load note revision', async (t) => {
  const { user, note } = t.context
  const { text, description } = noteBody()
  await axios.post(`/put/notes/${note._id}`, {
    text,
    description,
    token: user.token,
  }).catch(unwrapAndThrow)
  const { data: note1 } = await axios.post(`/get/notes/${note._id}/revision/0`).catch(unwrapAndThrow)
  t.is(note1.createdAt, note.createdAt)
  t.is(note1.text, note.text)
  t.is(note1.description, note.description)
  const { data: note2 } = await axios.post(`/get/notes/${note._id}/revision/1`).catch(unwrapAndThrow)
  t.is(note2.createdAt, note.createdAt)
  t.is(note2.text, text)
  t.is(note2.description, description)
})

test('should fail to delete note', async (t) => {
  t.plan(3)
  const { user, note } = t.context
  const { data: otherUser } = await axios.post('/users', {
    ...userBody(),
  }).catch(unwrapAndThrow)
  await axios.post(`/delete/notes/${note._id}`).catch(unwrapAndThrow).catch((err) => t.is(err.status, 401))
  await axios.post(`/delete/notes/${note._id}`, {
    token: otherUser.token,
  }).catch(unwrapAndThrow).catch((err) => t.is(err.status, 401))
  await axios.post(`/delete/notes/${randomObjectId()}`, {
    token: user.token,
  }).catch(unwrapAndThrow).catch((err) => t.is(err.status, 404))
})

test('should delete note', async (t) => {
  t.plan(2)
  const { user, note } = t.context
  // Neither should 404
  await axios.post(`/get/notes/${note._id}`).catch(unwrapAndThrow)
  await axios.post(`/get/notes/${note.index}`).catch(unwrapAndThrow)
  await axios.post(`/delete/notes/${note._id}`, {
    token: user.token,
  }).catch(unwrapAndThrow)
  // One should 404
  await axios.post(`/get/notes/${note.index}`).catch(unwrapAndThrow).catch((err) => t.is(err.status, 404))
  const { data: updated } = await axios.post(`/get/notes/${note._id}`).catch(unwrapAndThrow)
  t.is(updated.index, -1)
})

test('should load notes', async (t) => {
  const { user, note } = t.context
  const { data: notes } = await axios.post('/get/notes')
  t.pass()
})

test('should load notes by username', async (t) => {
  const { user, note } = t.context
  const { data: notes } = await axios.post(`/get/notes`, {
    usernames: JSON.stringify([user.username, 'badusername']),
  }).catch(unwrapAndThrow)
  t.is(notes.length, 1)
})

test('should fail to create bad reaction', async (t) => {
  t.plan(3)
  const { user, note } = t.context
  // reaction that is too long
  await axios.post(`/notes/${note._id}/reaction`, {
    reaction: randomstring(MAX_REACTION_LENGTH + 1),
    token: user.token,
  }).catch(unwrapAndThrow).catch(err => t.is(err.status, 400))
  // undefined reaction
  await axios.post(`/notes/${note._id}/reaction`, {
    token: user.token,
  }).catch(unwrapAndThrow).catch(err => t.is(err.status, 400))
  // bad note id
  await axios.post(`/notes/${randomObjectId()}/reaction`, {
    reaction: randomstring(MAX_REACTION_LENGTH),
    token: user.token,
  }).catch(unwrapAndThrow).catch(err => t.is(err.status, 404))
})

test('should update reaction', async (t) => {
  const { user, note } = t.context
  const reaction1 = randomstring(MAX_REACTION_LENGTH)
  const { data: note1 } = await axios.post(`/notes/${note._id}/reaction`, {
    reaction: reaction1,
    token: user.token,
  }).catch(unwrapAndThrow)
  t.is(note1.reactions[0].reaction, reaction1)
  const reaction2 = randomstring(MAX_REACTION_LENGTH)
  const { data: note2 } = await axios.post(`/notes/${note._id}/reaction`, {
    reaction: reaction2,
    token: user.token,
  }).catch(unwrapAndThrow)
  // Old reaction should be replaced
  t.is(note2.reactions.length, 1)
  t.is(note2.reactions[0].reaction, reaction2)
})

test('should fail to validate reaction', async (t) => {
  const { user, note } = t.context
  const { data: v1 } = await axios.post(`/notes/${note._id}/reaction/validate`, {
    token: user.token,
  }).catch(unwrapAndThrow)
  t.is(v1.reactionValid, false)
  t.is(v1.existingCount, 0)
  const { data: v2 } = await axios.post(`/notes/${note._id}/reaction/validate`, {
    reaction: randomstring(MAX_REACTION_LENGTH + 1),
    token: user.token,
  }).catch(unwrapAndThrow)
  t.is(v2.reactionValid, false)
  t.is(v2.existingCount, 0)
})

test('should validate reaction', async (t) => {
  const { user, note } = t.context
  const reaction = randomstring(MAX_REACTION_LENGTH)
  const { data: v1 } = await axios.post(`/notes/${note._id}/reaction/validate`, {
    reaction,
    token: user.token,
  }).catch(unwrapAndThrow)
  t.is(v1.reactionValid, true)
  t.is(v1.existingCount, 0)
  await axios.post(`/notes/${note._id}/reaction`, {
    reaction,
    token: user.token,
  }).catch(unwrapAndThrow)
  const { data: v2 } = await axios.post(`/notes/${note._id}/reaction/validate`, {
    reaction,
    token: user.token,
  }).catch(unwrapAndThrow)
  t.is(v2.reactionValid, true)
  t.is(v2.existingCount, 1)
})

test('should fail to load patches', async (t) => {
  const { user, note } = t.context
  await axios.post(`/get/notes/${randomObjectId()}/revisions`)
    .catch(unwrapAndThrow).catch(err => t.is(err.status, 404))
  await axios.post(`/get/notes/1481248284/revisions`)
    .catch(unwrapAndThrow).catch(err => t.is(err.status, 404))
})

test('should load patches', async (t) => {
  const { user, note } = t.context
  const { text, description } = noteBody()
  await axios.post(`/put/notes/${note._id}`, {
    text,
    description,
    token: user.token,
  }).catch(unwrapAndThrow)
  const { data: patches1 } = await axios.post(`/get/notes/${note.index}/revisions`).catch(unwrapAndThrow)
  const { data: patches2 } = await axios.post(`/get/notes/${note._id}/revisions`).catch(unwrapAndThrow)
  t.is(patches1.length, 2)
  t.is(patches2.length, 2)
})

test('should fail to load drafts', async (t) => {
  const { user, note } = t.context
  await axios.post('/get/notes/drafts').catch(unwrapAndThrow).catch(err => t.is(err.status, 401))
})

test('should load drafts', async (t) => {
  const { user, note } = t.context
  const { data: drafts1 } = await axios.post(`/get/notes/drafts`, {
    token: user.token,
  }).catch(unwrapAndThrow)
  t.is(drafts1.length, 0)
  await axios.post(`/put/notes/${note._id}`, {
    description: note.description,
    text: note.text,
    isDraft: true,
    token: user.token,
  }).catch(unwrapAndThrow)
  const { data: drafts2 } = await axios.post(`/get/notes/drafts`, {
    token: user.token,
  }).catch(unwrapAndThrow)
  t.is(drafts2.length, 1)
})

// assent to a gripping fantasy is called comprehension
