const mongoose = require('mongoose')

const NotePatchSchema = new mongoose.Schema(
  {
    noteId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    ownerId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    createdAt: {
      type: Date,
      required: true,
    },
    descriptionPatch: {
      type: String,
    },
    textPatch: {
      type: String,
    },
    index: {
      type: Number,
      required: true,
    }
  }
)

mongoose.model('NotePatch', NotePatchSchema)
