const mongoose = require('mongoose')

const BlockSchema = new mongoose.Schema(
  {
    number: {
      type: Number,
      required: true,
      index: {
        unique: true,
      },
    },
    createdAt: {
      type: Date,
      required: true,
    },
    processed: {
      type: Boolean,
      default: false,
    },
  }
)

BlockSchema.index({
  number: 1,
  processed: 1,
})

mongoose.model('Block', BlockSchema)
