FROM alpine:latest
MAINTAINER Chance Hudson

RUN apk add --update nodejs-npm python make git gcc g++

COPY . /src
WORKDIR /src

RUN npm ci --only=prod

CMD ["npm", "start"]
