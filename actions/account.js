const Web3 = require('web3')
const BN = require('bn.js')
const _ = require('lodash')
const mongoose = require('mongoose')
const Account = mongoose.model('Account')
const AccountTransaction = mongoose.model('AccountTransaction')
const User = mongoose.model('User')
const Subscription = mongoose.model('Subscription')
const Note = mongoose.model('Note')
const NoteRead = mongoose.model('NoteRead')
const Block = mongoose.model('Block')
const uuid = require('uuid')
const axios = require('axios')

const SUBSCRIPTION_DAYS = +process.env.SUBSCRIPTION_DAYS || 31
const SUBSCRIPTION_MS = +process.env.SUBSCRIPTION_MS || SUBSCRIPTION_DAYS * 24 * 60 * 60 * 1000

module.exports = {
  findOrCreateSubscriptionOffer,
  createPendingTransactions,
  updateBalanceDelta,
  updateBalance,
  loadEthPrice,
  toWei,
  toEther,
}

async function loadEthPrice() {
  const { data } = await axios('https://api.coinbase.com/v2/exchange-rates')
  const ETH_USD_RATE = data.data.rates.ETH
  const USD_ETH_RATE = Math.round(1e2 / ETH_USD_RATE) / 1e2
  return USD_ETH_RATE
}

function toWei(value) {
  const web3 = new Web3(process.env.GETH_URL)
  return web3.utils.toWei(value)
}

function toEther(value) {
  const web3 = new Web3(process.env.GETH_URL)
  const etherValue = web3.utils.fromWei(new BN(value))
  return parseFloat(etherValue).toFixed(5)
}

async function findOrCreateSubscriptionOffer(userId) {
  // 1 hour or the supplied subscription length (for test purposes)
  const maxOffset = Math.min(1000 * 60 * 60, SUBSCRIPTION_MS)
  const validTime = new Date(+new Date() - maxOffset)
  const [ subscription ] = await Promise.all([
    Subscription.findOne({
      active: false,
      settled: false,
      ownerId: userId,
      createdAt: {
        // Get the offer if created in the last hour
        $gt: validTime
      }
    }).exec(),
    Subscription.deleteMany({
      active: false,
      settled: false,
      accountTransactionId: {
        $exists: false,
      },
      ownerId: userId,
      createdAt: {
        $lte: validTime
      }
    }).exec()
  ])
  if (subscription) {
    // Bump the expiration
    await Subscription.updateOne({
      _id: subscription._id,
    }, {
      expiresAt: new Date(+new Date() + +SUBSCRIPTION_MS),
    })
    return subscription
  }
  const price = await loadEthPrice()
  const ethPrice = 4 / price
  const { _doc } = await Subscription.create({
    ownerId: userId,
    createdAt: new Date(),
    expiresAt: new Date(+new Date() + +SUBSCRIPTION_MS),
    exchangeRate: price,
    ethPrice,
    price: toWei(ethPrice.toString()),
  })
  return _doc
}

async function createPendingTransactions(_userId) {
  const userId = mongoose.Types.ObjectId(_userId)
  const notes = await Note.find({
    ownerId: userId,
  }).select('_id').exec()
  const reads = await NoteRead.find({
    noteId: {
      $in: _.map(notes, '_id')
    },
    ownerId: {
      $ne: userId,
    },
    lastReadAt: {
      $gte: new Date((+new Date()) - +SUBSCRIPTION_MS),
    },
  }).select('-viewIds').exec()
  const readsByOwnerId = _.groupBy(reads, 'ownerId')
  const subscriptions = await Subscription.find({
    active: true,
    ownerId: {
      $in: _.map(reads, 'ownerId'),
    },
  }).exec()
  const acceptedReads = []
  for (const subscription of subscriptions) {
    const reads = readsByOwnerId[subscription.ownerId]
    for (const read of reads) {
      if (
        subscription.createdAt <= read.lastReadAt &&
        subscription.expiresAt >= read.lastReadAt
      ) {
        acceptedReads.push(Object.assign(read, {
          subscription,
          maturesAt: subscription.expiresAt
        }))
      }
    }
  }
  const acceptedReadsByOwnerId = _.groupBy(acceptedReads, 'ownerId')
  const users = await User.find({
    _id: {
      $in: _.keys(acceptedReadsByOwnerId),
    },
  }).select(['_id', 'username'])
  const transactions = await AccountTransaction.find({
    ownerId: userId,
    subscriptionId: {
      $in: _.map(subscriptions, '_id'),
    },
    type: 'pending',
  }).exec()
  const txBySubscriptionId = _.keyBy(transactions, 'subscriptionId')
  await Promise.all(_.map(acceptedReadsByOwnerId, async (_reads, ownerId) => {
    const { subscription, maturesAt } = _reads[0]
    const ownedNotes = await Note.find({
      ownerId,
      index: {
        $gte: 0,
      },
      isUnmonetized: {
        $ne: true,
      }
    }).select('_id').exec()
    const totalReadCount = await NoteRead.countDocuments({
      ownerId,
      createdAt: {
        $gte: subscription.createdAt,
      },
      noteId: {
        $not: {
          $in: _.map(ownedNotes, '_id'),
        }
      },
    }).exec()
    const wei = new BN(subscription.price)
    // Take 5% (1/20)
    const platformWei = wei.div(new BN(20))
    const remainingWei = wei.sub(platformWei)
    // Estimate how many notes user will read
    const subscriptionCompletedPercent = ((+new Date()) - +subscription.createdAt) / (+subscription.expiresAt - +subscription.createdAt)
    const estimatedTotalReads = (totalReadCount || 1) / Math.min(1, subscriptionCompletedPercent)
    // Take X% for each note
    const noteWei = remainingWei.div(new BN(estimatedTotalReads))
    const estimatedAmount = noteWei.mul(new BN(_reads.length))

    if (txBySubscriptionId[subscription._id]) {
      await AccountTransaction.updateOne({
        subscriptionId: subscription._id,
        type: 'pending',
        ownerId: userId,
      }, {
        maturesAt,
        // new estimated amount
        amount: estimatedAmount.toString()
      })
    } else {
      const { _doc } = await AccountTransaction.create({
        maturesAt,
        ownerId: userId,
        createdAt: new Date(),
        state: 0,
        blockNumber: 0,
        blockTimestamp: new Date(),
        transactionId: uuid.v4(),
        type: 'pending',
        subscriptionId: subscription._id,
        subscriptionOwnerId: subscription.ownerId,
        amount: estimatedAmount.toString()
      })
      txBySubscriptionId[subscription._id] = _doc
    }
  }))
}

async function updateBalanceDelta(ownerId) {
  const account = await Account.findOne({
    ownerId,
  }).select('-privateKeyEncrypted').exec()
  if (!account) {
    throw new Error(`Unable to find account with ownerId "${ownerId}"`)
  }
  // Find transactions affecting delta
  const transactions = (await Promise.all([
    AccountTransaction.find({
      ownerId,
      accountId: account._id,
      type: {
        $in: ['payment', 'dividend'],
      },
    }).select(['type', 'state', 'amount']).exec(),
    AccountTransaction.find({
      // from: account.address,
      $or: [
        {
          from: account.address,
        }, {
          ownerId,
        }
      ],
      type: 'payout',
      state: 2,
    }).select(['type', 'from', 'state', 'amount', 'ownerId', 'gasAmount']).exec(),
  ])).flat()
  let sum = new BN(0)
  for (const tx of transactions) {
    // Add gas value so amount is accurate
    if (tx.type === 'payout' && tx.from === account.address) {
      const gasWei = (new BN(tx.gas)).mul(new BN(tx.gasPrice))
      sum = sum.add(new BN(tx.amount)).add(gasWei)
    }
    if (tx.type === 'payout' && tx.ownerId.toString() === ownerId.toString()) {
      const gasWei = (new BN(tx.gas)).mul(new BN(tx.gasPrice))
      sum = sum.sub(new BN(tx.amount)).sub(gasWei)
    }
    if (tx.type === 'payment') {
      // Logical, no gas
      sum = sum.sub(new BN(tx.amount))
    }
    if (tx.type === 'dividend') {
      // Logical, no gas
      sum = sum.add(new BN(tx.amount))
    }
  }
  if (sum.toString() === account.balanceDelta) {
    return
  }
  await Account.updateOne({
    _id: account._id,
  }, {
    balanceDelta: sum.toString(),
    balanceDeltaEther: toEther(sum),
  })
}

async function updateBalance(ownerId, _account) {
  const web3 = new Web3(process.env.GETH_URL)
  const account = _account ? _account : (await Account.findOne({
    ownerId,
  }).exec())
  if (account === null) {
    return
  }
  const latestBlock = await Block.findOne({
    processed: true,
  }).sort({ number: 'desc' }).select('number').exec()
  const transactionCount = await web3.eth.getTransactionCount(account.address, latestBlock.number)
  const balance = new BN(await web3.eth.getBalance(account.address, latestBlock.number))
  await Account.updateOne({
    _id: account._id,
  }, {
    balance,
    balanceEther: toEther(balance),
    transactionCount,
    updatedAt: new Date(),
  })
}
