const test = require('ava')
const axios = require('axios')
const Web3 = require('web3')
const BN = require('bn.js')
const {
  userBody,
  noteBody,
  startServer,
  randomstring,
  unwrapAndThrow,
  deposit,
  createUser,
} = require('./helpers')

let _shutdown
test.before(async (t) => {
  const { port, shutdown } = await startServer()
  _shutdown = shutdown
  axios.defaults.baseURL = `http://localhost:${port}`
})

test.after(async (t) => {
  await _shutdown()
})

test.beforeEach(async (t) => {
  const user = await createUser(axios)
  const { data: note } = await axios.post('/notes', {
    ...noteBody(),
    token: user.token,
  }).catch(unwrapAndThrow)
  t.context.user = user
  t.context.note = note
})

test('should withdraw from own account', async (t) => {
  const { user } = t.context
  const { data: account } = await axios.post('/get/account', {
    token: user.token,
  }).catch(unwrapAndThrow)
  // 30 ether
  const amount = '30000000000000000000'
  await deposit(account.address, amount)
  await axios.post(`/put/accounts/${account._id}`, {
    payoutAddress: '0x0000000000000000000000000000000000000000',
    token: user.token,
  }).catch(unwrapAndThrow)
  // Wait for payout
  let block = 0
  const txIds = {}
  for (;;) {
    await new Promise(r => setTimeout(r, 4000))
    const { data: tx } = await axios.post('/get/transactions', {
      token: user.token,
    }).catch(unwrapAndThrow)
    // console.log(tx.transactions.filter(_tx => _tx.accountId === account._id && _tx.type === 'payout'))
    if (tx.transactions.find((_tx) =>
      _tx.type === 'refund' && _tx.from === account.address && _tx.state === 2
    )) {
      break
    }
  }
  const { data: __tx } = await axios.post('/get/transactions', {
    token: user.token,
  }).catch(unwrapAndThrow)
  t.pass()
})

test('should create payout', async (t) => {
  const { user, note } = t.context
  // create 5 users and have them view the note
  const userCount = 5
  t.plan(userCount + 1)
  const userItt = Array.apply(null, Array(userCount)).map(() => {})
  const users = await Promise.all(
    userItt.map(() => createUser(axios).catch(unwrapAndThrow)),
  )
  // Iterable array of undefined values
  const count = 15
  const itt = Array.apply(null, Array(count)).map(() => {})
  for (const _user of users) {
    let start = +(new Date()) - (count * 1000)
    let scrollOffset = 0
    const { data: views } = await axios.post('/noteviews', {
      token: _user.token,
      views: itt.map(() => ({
        noteId: note._id,
        createdAt: start,
        noteWidth: 100,
        noteHeight: 100,
        viewportWidth: 100,
        viewportHeight: 500,
        scrollOffset,
        // Just for iterating the date in the loop
        __iterator: (start += 1000) && (scrollOffset += 1),
      }))
    })
    t.is(views.createdCount, count)
  }
  // After view creation wait for payout
  // Keep the process alive until the payout occurs
  const waitTime = +process.env.SUBSCRIPTION_MS + 10000
  await new Promise(r => setTimeout(r, waitTime))
  const { data: account } = await axios.post('/get/account', {
    token: user.token,
  }).catch(unwrapAndThrow)
  await axios.post(`/put/accounts/${account._id}`, {
    token: user.token,
    payoutAddress: '0x0000000000000000000000000000000000000000',
  }).catch(unwrapAndThrow)
  // Wait for payout calculation
  const web3 = new Web3(process.env.GETH_URL)
  let _count = 0
  for (;;) {
    await new Promise(r => setTimeout(r, 10000))
    const { data } = await axios.post('/get/transactions', {
      token: user.token
    }).catch(unwrapAndThrow)
    const _account = data.account
    // console.log(data.transactions)
    const { data: subscriptionPrice } = await axios.post('/get/subscription/price', {
      token: user.token,
    }).catch(unwrapAndThrow)
    const gas = new BN(await web3.eth.estimateGas({
      from: _account.address,
      to: _account.payoutAddress,
    }))
    const gasPrice = new BN(await web3.eth.getGasPrice())
    const gasWei = gas.mul(gasPrice)
    const gasEther = parseFloat(web3.utils.fromWei(gasWei)).toFixed(5)
    // See daemons/payout.js line 86 and 145
    const baseEtherCost = +gasEther + 0.001 + (1.5 * +subscriptionPrice.etherPrice)
    if (_account.balanceDeltaEther <= baseEtherCost) {
      t.pass()
      return
    }
    if (++_count >= 5) {
      t.assert(false, 'Account was not paid out')
      return
    }
  }
})
