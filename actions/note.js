const mongoose = require('mongoose')
const Note = mongoose.model('Note')
const NoteView = mongoose.model('NoteView')
const NoteRead = mongoose.model('NoteRead')
const NotePatch = mongoose.model('NotePatch')
const User = mongoose.model('User')
const Subscription = mongoose.model('Subscription')
const _ = require('lodash')
const diff_match_patch = require('diff-match-patch')

module.exports = {
  updateAll,
  updateNotePatchCount,
  updateNoteReadCount,
  updateReadsForNote,
  createPatch,
  loadNoteRevision,
}

async function createPatch(noteId, data) {
  const { _doc } = await NotePatch.create({
    createdAt: new Date(),
    index: -2,
    noteId,
    ownerId: data.ownerId,
    descriptionPatch: '',
    textPatch: '',
  })
  // Wait 1 second to account for variation in _id
  // https://docs.mongodb.com/manual/reference/method/ObjectId/#ObjectIDs-BSONObjectIDSpecification
  await new Promise(r => setTimeout(r, 1000))
  const patchIndex = await NotePatch.countDocuments({
    _id: {
      // Use lt to start at 0
      $lt: _doc._id,
    },
    index: {
      $gte: 0,
    },
    noteId,
  }).exec()
  // Calculate note at last patch, then patch
  let lastNote = {
    text: '',
    description: '',
  }
  if (patchIndex > 0) {
    lastNote = await loadNoteRevision(noteId, patchIndex - 1)
  }
  const dmp0 = new diff_match_patch()
  const descriptionDiff = dmp0.diff_main(lastNote.description, data.description)
  dmp0.diff_cleanupSemantic(descriptionDiff)
  const _descriptionPatch = dmp0.patch_make(lastNote.description, descriptionDiff)
  const dmp1 = new diff_match_patch()
  const textDiff = dmp1.diff_main(lastNote.text, data.text)
  dmp1.diff_cleanupSemantic(textDiff)
  const _textPatch = dmp1.patch_make(lastNote.text, textDiff)
  const descriptionPatch = dmp0.patch_toText(_descriptionPatch)
  const textPatch = dmp1.patch_toText(_textPatch)

  await NotePatch.updateOne({
    _id: _doc._id,
  }, {
    index: patchIndex,
    textPatch,
    descriptionPatch,
  })
  const patchCount = await NotePatch.countDocuments({
    noteId,
    index: {
      $gte: 0,
    },
  }).exec()
  const { ok, nModified } = await Note.updateOne({
    _id: noteId,
    ownerId: data.ownerId,
  }, {
    updatedAt: new Date(),
    text: data.text,
    description: data.description,
    patchCount,
  })
  if (nModified !== 1) {
    throw new Error('Note was not updated')
  }
}

async function loadNoteRevision(noteId, _index) {
  const index = +_index
  if (isNaN(index)) {
    throw new Error(`Non-number index supplied ${_index}`)
  }
  const note = await Note.findOne({
    _id: noteId,
  }).populate('owner').lean().exec()
  if (note === null) {
    throw new Error(`Note does not exist`)
  }
  const patches = await NotePatch.find({
    noteId,
    index: {
      $lte: index,
      $gte: 0,
    },
  }).exec()
  if (patches.length !== index + 1) {
    throw new Error(`Unexpected number of patches ${patches.length}/${index + 1}`)
  }
  let text = ''
  let description = ''
  for (const patch of patches) {
    const dmp = new diff_match_patch()
    const textPatches = dmp.patch_fromText(patch.textPatch)
    const [newText, results] = dmp.patch_apply(textPatches, text)
    const descriptionPatches = dmp.patch_fromText(patch.descriptionPatch)
    const [ newDescription, _results ] = dmp.patch_apply(descriptionPatches, description)
    text = newText
    description = newDescription
  }
  return {
    ...note,
    text,
    description,
  }
}

/* istanbul ignore next */
async function generatePatchIndexes() {
  const notes = await Note.find({}).exec()
  for (const note of notes) {
    console.log(`Updating note ${notes.indexOf(note)}/${notes.length}`)
    const patches = await NotePatch.find({
      noteId: note._id,
    }).exec()
    for (const patch of patches) {
      console.log(`Patch ${patches.indexOf(patch)}/${patches.length}`)
      await NotePatch.updateOne({
        _id: patch._id,
      }, {
        index: patches.indexOf(patch) + 1,
      })
    }
  }
  console.log('done')
}

/* istanbul ignore next */
async function updateAll() {
  const notes = await Note.find({
    index: {
      $gte: 0,
    }
  }).exec()
  const users = await User.find({}).exec()
  let i = 0
  const total = notes.length * users.length
  const { nModified } = await Note.updateMany({}, {
    readCount: 0,
  })
  console.log(`Reset ${nModified} counts`)
  await NoteRead.deleteMany({})
  for (const note of notes) {
    console.log(`Processing note ${notes.indexOf(note)} of ${notes.length}`)
    console.log(`Updating counts for ${users.length} users`)
    for (const user of users) {
      await updateReadsForNote(note._id, user._id)
    }
    await updateReadsForNote(note._id)
    console.log('Updating note document')
    await updateNoteReadCount(note._id)
    console.log()
  }
  console.log('done')
}

/* istanbul ignore next */
async function updateNotePatchCount(_noteId) {
  const noteId = mongoose.Types.ObjectId(_noteId)
  const count = await Note.countDocuments({
    _id: noteId,
  }).exec()
  if (count === 0) {
    throw new Error(`Could not find note with id "${_noteId}"`)
  }
  const patchCount = await NotePatch.countDocuments({
    noteId,
    index: {
      $gte: 0,
    },
  }).exec()
  await Note.updateOne({
    _id: noteId,
  }, {
    patchCount,
  })
}

async function updateNoteReadCount(_noteId) {
  const noteId = mongoose.Types.ObjectId(_noteId)
  await updateReadsForNote(_noteId)
  const note = await Note.findOne({
    _id: noteId,
  }).exec()
  if (note === null) {
    throw new Error(`Could not find note with id "${_noteId}"`)
  }
  const found = await NoteRead.aggregate([
    { $match: { noteId, } },
    {
      $group: {
        _id: noteId,
        totalPaidReadCount: { $sum: '$paidReadCount' },
        totalReadPercent: { $sum: '$readPercent' },
        totalReadCount: { $sum: '$readCount' },
        totalReadSeconds: { $sum: '$totalReadSeconds' },
      }
    }
  ]).exec()
  if (found.length === 0) {
    await Note.updateOne({
      _id: noteId,
    }, {
      readCount: 0,
    })
    return
  }
  const [{
    totalReadCount,
    totalReadPercent,
    totalReadSeconds,
    totalPaidReadCount
  }] = found
  await Note.updateOne({
    _id: noteId,
  }, {
    readCount: totalPaidReadCount,
  })
}

// Update the read count for a note
// If user is undefined it does so for anonymous reads
async function updateReadsForNote(noteId, _userId) {
  const _id = mongoose.Types.ObjectId(noteId)
  let userId = null
  if (_userId) {
    userId = mongoose.Types.ObjectId(_userId)
  }
  const note = await Note.findOne({
    _id,
  }).exec()
  if (note === null) {
    throw new Error(`Unable to find note with id ${noteId}`)
  }
  const userRead = await NoteRead.findOne({
    noteId: _id,
    ownerId: userId,
  }).exec()
  const views = await NoteView.find({
    noteId: _id,
    ownerId: userId,
    createdAt: {
      $gt: userRead ? userRead.updatedAt : 0,
    }
  }).sort({ createdAt: 'asc' }).exec()
  if (views.length === 0) return
  const estimatedReadingTime = Math.floor(note.text.length / 30)
  const reads = []
  let currentRead
  for (const view of views) {
    // Reset if not continuous session
    if (currentRead && view.createdAt - currentRead.finishedAt > 3000) {
      reads.push(currentRead)
      currentRead = undefined
    }
    if (currentRead && currentRead.views.length > 0) {
      // If no scrolling occurs for 30 seconds reset the session
      const lastView = currentRead.views[currentRead.views.length - 1]
      if (+lastView.createdAt - +currentRead.lastOffsetAt > 30000) {
        // 30 seconds without scrolling, session timeout
        reads.push(currentRead)
        currentRead = undefined
      }
      // If the change in offset is more than 20% of the document, discard but
      // continue read session
      if (Math.abs(lastView.lastOffset - view.scrollOffset) > (view.noteHeight || view.clientHeight) * 0.2) {
        continue
      }
    }
    if (!currentRead) {
      currentRead = {
        createdAt: view.createdAt,
        finishedAt: view.createdAt,
        minOffset: Infinity,
        maxOffset: 0,
        lastOffset: view.scrollOffset,
        lastOffsetAt: view.createdAt,
        views: [],
      }
    }
    if (currentRead.lastOffset !== view.scrollOffset) {
      currentRead.lastOffsetAt = view.createdAt
    }
    if (view.scrollOffset < currentRead.minOffset) {
      currentRead.minOffset = view.scrollOffset
    }
    if (view.scrollOffset > currentRead.maxOffset) {
      currentRead.maxOffset = view.scrollOffset
    }
    currentRead.lastOffset = view.scrollOffset
    currentRead.finishedAt = view.createdAt
    currentRead.views.push(view)
  }
  if (currentRead) reads.push(currentRead)
  const acceptedReads = []
  let readPercent = 0
  let totalReadSeconds = 0
  for (const read of reads) {
    const distance = read.maxOffset - read.minOffset
    if (+read.finishedAt === +read.createdAt) continue
    const viewportHeight = _.sumBy(read.views, (view) => {
      return view.viewportHeight
    }) / read.views.length
    const docHeight = _.sumBy(read.views, (view) => {
      return view.noteHeight || view.clientHeight
    }) / read.views.length
    const readingTime = (+read.finishedAt - +read.createdAt) / 1000
    const timePercent = readingTime / estimatedReadingTime
    const scrollPercent = Math.min(docHeight, Math.max(distance, viewportHeight)) / docHeight
    // console.log(note.index)
    // console.log('reading time %', timePercent)
    // console.log('scroll %', scrollPercent)
    acceptedReads.push(read)
    // Use a minimum of 200px to assume the user reads in place
    readPercent += (timePercent + scrollPercent) / 2
    totalReadSeconds += readingTime
  }
  // Don't update if the percent is 0, leave the views for future processing
  if (readPercent === 0) return
  // console.log('adding ', readPercent)
  const viewIds = _.chain(acceptedReads)
    .map('views')
    .flatten()
    .map('_id')
    .map(mongoose.Types.ObjectId)
    .value()
  const lastPercent = userRead ? userRead.readPercent : 0
  const newPercent = lastPercent + readPercent
  const newCount = Math.floor(newPercent)
  const lastCount = Math.floor(lastPercent)
  const existingRead = await NoteRead.findOne({
    noteId: _id,
    ownerId: userId,
  }).select('-viewIds').exec()
  if (existingRead === null) {
    await NoteRead.create({
      noteId: _id,
      ownerId: userId,
      createdAt: new Date(),
      updatedAt: new Date(),
    })
  }
  let paidRead = false
  if (userId && existingRead) {
    const subscription = await Subscription.findOne({
      ownerId: userId,
      active: true,
    }).exec()
    if (subscription) {
      const readDates = existingRead.readDates.filter((d) => {
        return +d >= +subscription.createdAt
      })
      if (readDates.length === 0 && newCount > lastCount) {
        // just encountered first read for subscription
        paidRead = true
      }
    }
  } else if (!existingRead && newCount > lastCount) {
    paidRead = true
  }
  await NoteRead.updateOne({
    noteId: _id,
    ownerId: userId,
  }, {
    updatedAt: new Date(),
    readPercent: newPercent,
    readCount: newCount,
    ...(newCount > lastCount ? {
      lastReadAt: new Date(),
    } : {}),
    $push: {
      viewIds: {
        $each: _.map(viewIds, i => i.toString()),
      },
      ...(newCount > lastCount ? {
        readDates: new Date(),
      } : {}),
    },
    $inc: {
      totalReadSeconds: Math.floor(totalReadSeconds),
      ...(paidRead ? {
        paidReadCount: 1,
      } : {}),
    },
  })
  await updateNoteReadCount(_id)
}
