const jwt = require('jsonwebtoken')
const mongoose = require('mongoose')
const Request = mongoose.model('Request')

// Time and log the request asynchronously
module.exports = (app) => {
  app.use(timeRequest)
}

async function timeRequest(req, res, next) {
  const startTime = new Date()
  let rs
  const reqPromise = new Promise((_rs) => rs = _rs)
  const ip = req.headers['x-forwarded-for']
  const _path = req.path
  const method = req.method
  const token = req.query.token || req.body.token
  res.on('finish', () => {
    rs()
  })
  next()
  let userId
  try {
    const user = jwt.verify(token, process.env.WEB_TOKEN_SECRET)
    if (user._id) {
      userId = mongoose.Types.ObjectId(user._id)
    }
  } catch (err) {}
  try {
    const { _doc } = await Request.create({
      ownerId: userId,
      createdAt: startTime,
      ip,
      path: _path,
      method,
    })
    await reqPromise
    const completedAt = new Date()
    await Request.updateOne({
      _id: _doc._id,
    }, {
      completedAt,
      executionTimeMs: (+completedAt - +startTime),
    })
  } catch (err) {
    console.log('Request creation failed')
    console.log(err)
  }
}
