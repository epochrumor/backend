const mongoose = require('mongoose')

const SubscriptionSchema = new mongoose.Schema(
  {
    ownerId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    createdAt: {
      type: Date,
      required: true,
    },
    exchangeRate: {
      type: Number,
      required: true,
    },
    ethPrice: {
      type: Number,
      required: true,
    },
    price: {
      type: String,
      required: true,
    },
    expiresAt: {
      type: Date,
      required: true,
    },
    active: {
      type: Boolean,
      required: true,
      default: false,
    },
    settled: {
      type: Boolean,
      required: true,
      default: false,
    },
    accountTransactionId: {
      type: mongoose.Schema.Types.ObjectId,
    },
  }
)

mongoose.model('Subscription', SubscriptionSchema)
