const test = require('ava')
const axios = require('axios')
const {
  MIN_USERNAME_LENGTH,
  MAX_USERNAME_LENGTH,
  MIN_PASSWORD_LENGTH,
} = require('../constants')
const {
  userBody,
  startServer,
  randomstring,
  unwrapAndThrow
} = require('./helpers')
const randomObjectId = require('random-objectid')

let _shutdown
test.before(async (t) => {
  const { port, shutdown } = await startServer()
  _shutdown = shutdown
  axios.defaults.baseURL = `http://localhost:${port}`
})

test.after(async (t) => {
  await _shutdown()
})

test.beforeEach(async (t) => {
  const { data: user } = await axios.post('/users', {
    ...userBody()
  }).catch(unwrapAndThrow)
  t.context.user = user
})

/**
 * Shared api functions
 **/
async function createUser(options = {}) {
  const { data } = await axios.post('/users', {
    ...userBody(),
    email: `${randomstring()}@email.com`,
    ...options,
  }).catch(unwrapAndThrow)
  return data
}
module.exports.createUser = createUser

test('should fail to create user without password', async (t) => {
  await axios.post('/users', {
    ...userBody(),
    password: '',
  }).catch(unwrapAndThrow).catch((err) => t.is(err.status, 400))
})

test('should fail to create user without username', async (t) => {
  await axios.post('/users', {
    ...userBody(),
    username: '',
  }).catch(unwrapAndThrow).catch((err) => t.is(err.status, 400))
})

test('should fail to create user without email', async (t) => {
  await axios.post('/users', {
    ...userBody(),
    email: '',
  }).catch(unwrapAndThrow).catch((err) => t.is(err.status, 400))
})

test('should fail to create user with taken username', async (t) => {
  const { user } = t.context
  await axios.post('/users', {
    ...userBody(),
    username: user.username.toUpperCase(),
  }).catch(unwrapAndThrow).catch((err) => t.is(err.status, 400))
})

test('should fail to create user with bad email', async (t) => {
  const { user } = t.context
  await axios.post('/users', {
    ...userBody(),
    email: 'test',
  }).catch(unwrapAndThrow).catch((err) => t.is(err.status, 400))
})

test('should fail to create user with bad username', async (t) => {
  t.plan(2)
  await axios.post('/users', {
    ...userBody(),
    username: randomstring(MIN_USERNAME_LENGTH - 1),
  }).catch(unwrapAndThrow).catch((err) => t.is(err.status, 400))
  await axios.post('/users', {
    ...userBody(),
    username: randomstring(MAX_USERNAME_LENGTH + 1),
  }).catch(unwrapAndThrow).catch((err) => t.is(err.status, 400))
})

test('should fail to create user with bad password', async (t) => {
  await axios.post('/users', {
    ...userBody(),
    password: randomstring().slice(0, MIN_PASSWORD_LENGTH - 1),
  }).catch(unwrapAndThrow).catch((err) => t.is(err.status, 400))
})

test('should create user', async (t) => {
  const { data } = await axios.post('/users', {
    ...userBody(),
  }).catch(unwrapAndThrow)
  t.assert(data.token)
})

test('should login', async (t) => {
  const userData = userBody()
  const user = await createUser({
    ...userData,
  })
  const { data } = await axios.post('/users/login', {
    ...userData,
  }).catch(unwrapAndThrow)
  t.is(data._id, user._id)
})

test('should fail to login', async (t) => {
  t.plan(2)
  await axios.post('/users/login', {
    ...userBody(),
  }).catch(unwrapAndThrow).catch((err) => t.is(err.status, 404))

  const password = randomstring()
  const { username } = await createUser({ password })
  await axios.post('/users/login', {
    username,
    password: randomstring(),
  }).catch(unwrapAndThrow).catch((err) => t.is(err.status, 401))
})

test('should fail to load user', async (t) => {
  const { user } = t.context
  await axios.post(`/get/users/${randomObjectId()}`)
    .catch(unwrapAndThrow).catch((err) => t.is(err.status, 404))
})

test('should load user', async (t) => {
  const { user } = t.context
  t.assert(!user.passwordHash, 'Unexpected password hash received')
  const { data } = await axios.post(`/get/users/${user._id}`)
  t.assert(!data.passwordHash, 'Unexpected password hash received')
  t.assert(data._id)
})

test('should load authed user', async (t) => {
  const { user } = t.context
  const { data } = await axios.post(`/get/users/authed`, {
    token: user.token,
  }).catch(unwrapAndThrow)
  t.is(user._id, data._id)
})

test('should refresh auth token', async (t) => {
  const { user } = t.context
  const { data } = await axios.post('/put/users/token/refresh', {
    token: user.token,
  }).catch(unwrapAndThrow)
  t.assert(data.token)
  t.assert(user.token !== data.token)
  await axios.post('/get/users/authed', {
    token: user.token,
  }).catch(unwrapAndThrow)
})

test('should fail to update user', async (t) => {
  t.plan(2)
  const { user } = t.context
  await axios.post('/put/users/edit', {
    username: randomstring(MAX_USERNAME_LENGTH + 1),
    token: user.token,
  }).catch(unwrapAndThrow).catch(err => t.is(err.status, 422))
  await axios.post('/put/users/edit', {
    username: randomstring(MIN_USERNAME_LENGTH - 1),
    token: user.token,
  }).catch(unwrapAndThrow).catch(err => t.is(err.status, 422))
})

test('should update user', async (t) => {
  const { user } = t.context
  const username = randomstring(MAX_USERNAME_LENGTH)
  await axios.post('/put/users/edit', {
    username,
    token: user.token,
  }).catch(unwrapAndThrow)
  const { data: user1 } = await axios.post('/get/users/authed', {
    token: user.token,
  }).catch(unwrapAndThrow)
  t.is(user1.username.toLowerCase(), username.toLowerCase())
  const bio = randomstring()
  await axios.post('/put/users/edit', {
    bio,
    token: user.token,
  }).catch(unwrapAndThrow)
  const { data: user2 } = await axios.post('/get/users/authed', {
    token: user.token,
  }).catch(unwrapAndThrow)
  t.is(user2.bio, bio)
  await axios.post('/put/users/edit', {
    token: user.token,
  }).catch(unwrapAndThrow)
  const { data: user3 } = await axios.post('/get/users/authed', {
    token: user.token,
  }).catch(unwrapAndThrow)
  t.assert(user3.updatedAt !== user2.updatedAt)
})

test('should validate user', async (t) => {
  const { user } = t.context
  const { data } = await axios.post('/users/validate', {
  }).catch(unwrapAndThrow)
  t.is(data.username, false)
  t.is(data.password, false)
  t.is(data.email, false)
})
