const mongoose = require('mongoose')
const User = mongoose.model('User')
const Account = mongoose.model('Account')
const Subscription = mongoose.model('Subscription')
const AccountTransaction = mongoose.model('AccountTransaction')
const NoteRead = mongoose.model('NoteRead')
const Note = mongoose.model('Note')
const Web3 = require('web3')
const BN = require('bn.js')
const { updateReadsForNote } = require('../actions/note')
const _ = require('lodash')
const uuid = require('uuid')
const {
  findOrCreateSubscriptionOffer,
  updateBalanceDelta,
  updateBalance,
} = require('../actions/account')
const randomObjectId = require('random-objectid')
const { acquireLock, releaseLock } = require('../actions/account_lock')
const { addShutdownListener } = require('../shutdown')

let shuttingDown = false
// Call this when finished
let shutdownFinished
function shutdown() {
  return new Promise((rs) => {
    shutdownFinished = rs
    shuttingDown = true
  })
}
addShutdownListener(shutdown)

// Start a run loop
;(async () => {
  let errorCount = 0
  for (;;) {
    try {
      // find subscriptions that ended
      const subscriptions = await Subscription.find({
        active: true,
        settled: false,
        expiresAt: {
          $lt: new Date()
        },
      }).select(['_id', 'ownerId']).exec()
      for (const { _id, ownerId } of subscriptions) {
        const account = await Account.findOne({
          ownerId,
        }).select('_id').exec()
        if (!account) {
          console.log('No account for user account')
          continue
        }
        const locked = await acquireLock(account._id, true)
        if (!locked) {
          continue
        }
        await createDividends(_id)
        await autorenew(ownerId)
        await releaseLock(account._id)
      }
    } catch (err) {
      /* istanbul ignore next */
      {
        if (++errorCount >= 3) {
          console.log('too many errors, exiting')
          console.log(err)
          process.exit(1)
        }
        console.log(`dividend daemon error ${errorCount}/3`)
        console.log(err)
      }
    } finally {
      if (shuttingDown) {
        shutdownFinished()
        break
      }
      await new Promise(r => setTimeout(r, 2500))
    }
  }
})()

// Idempotently create dividend transactions for a subscription
// Outcome dependent on NoteReads data
async function createDividends(_subscriptionId) {
  const subscriptionId = mongoose.Types.ObjectId(_subscriptionId)
  const { nModified } = await Subscription.updateOne({
    _id: subscriptionId,
    active: true,
    settled: false,
  }, {
    active: false,
  }).exec()
  if (nModified === 0) {
    // Another thread settled before we acquired the lock
    return
  }
  const subscription = await Subscription.findOne({
    _id: subscriptionId,
  }).exec()
  if (subscription === null) {
    throw new Error('Subscription does not exist')
  }
  if (subscription.settled) {
    return
  }
  if (+new Date(subscription.expiresAt) > +new Date()) {
    throw new Error('Subscription has not ended yet')
  }
  const readsToUpdate = await NoteRead.find({
    ownerId: subscription.ownerId,
    lastReadAt: {
      $gte: subscription.createdAt,
      $lte: subscription.expiresAt,
    },
    updatedAt: {
      $lte: subscription.expiresAt,
    }
  }).select(['_id', 'noteId']).exec()
  // console.log(`Found ${readsToUpdate.length} reads to update`)
  if (readsToUpdate.length > 0) {
    // This should only be hit in edge cases
    // e.g. views have been posted by reads not yet updated
    await Promise.all(readsToUpdate.map((read) => {
      return updateReadsForNote(read.noteId)
    }))
  }
  // Threshold implementation
  const reads = await NoteRead.find({
    ownerId: subscription.ownerId,
    lastReadAt: {
      $gte: subscription.createdAt,
      $lte: subscription.expiresAt,
    }
  }).select(['_id', 'noteId']).exec()
  if (reads.length === 0) {
    // short circuit out
    await Subscription.updateOne({
      _id: subscription._id,
    }, {
      settled: true,
      active: false,
    })
    return
  }
  const notes = await Note.find({
    _id: {
      $in: _.map(reads, 'noteId'),
    },
    // exclude notes owned by subscriber
    ownerId: {
      $ne: subscription.ownerId,
    },
    index: {
      $gte: 0,
    },
    isUnmonetized: {
      $ne: true,
    },
  }).select(['_id', 'ownerId']).exec()
  // 5% credited to epochrumor (platform dividend)
  // "i'm going to build my own bank account i guess"
  const wei = new BN(subscription.price)
  // Take 5% (1/20)
  const platformWei = wei.div(new BN(20))
  const remainingWei = wei.sub(platformWei)
  if (notes.length === 0) {
    // Pay back to subscribing user if no reads
    // Push a stub note
    notes.push({
      // Make sure this isn't used
      _id: randomObjectId(),
      ownerId: subscription.ownerId,
    })
  }
  // Take X% for each note
  const noteWei = remainingWei.div(new BN(notes.length))
  const notesByOwnerId = _.groupBy(notes, 'ownerId')
  const accounts = await Account.find({
    ownerId: {
      $in: _.map(notes, 'ownerId')
    }
  }).select(['_id', 'ownerId', 'address']).exec()
  const accountsByOwnerId = _.chain(accounts)
    .uniqBy('ownerId')
    .keyBy('ownerId')
    .value()
  const promises = _.map(notesByOwnerId, async (notes, ownerId) => {
    const account = accountsByOwnerId[ownerId]
    await AccountTransaction.create({
      accountId: account && account._id,
      to: account && account.address,
      ownerId,
      createdAt: new Date(),
      state: 0,
      blockNumber: 0,
      blockTimestamp: new Date(),
      transactionId: uuid.v4(),
      type: 'dividend',
      subscriptionId: subscription._id,
      subscriptionOwnerId: subscription.ownerId,
      amount: noteWei.mul(new BN(notes.length)).toString()
    })
  })
  await Promise.all(promises)
  const { nModified: _nModified } = await Subscription.updateOne({
    _id: subscription._id,
    settled: false,
  }, {
    settled: true,
  })
  if (_nModified === 0) {
    // Abort, this Subscription has already been settled
    throw new Error(`Failed to mark subscription "${subscription._id.toString()}" as settled`)
  }
  return
}

async function autorenew(ownerId) {
  const activeSubscription = await Subscription.findOne({
    active: true,
    ownerId,
  }).select(['_id']).exec()
  if (activeSubscription) {
    return
  }
  const [ subscriptionOffer ] = await Promise.all([
    findOrCreateSubscriptionOffer(ownerId),
    updateBalanceDelta(ownerId),
    updateBalance(ownerId),
  ])
  const account = await Account.findOne({
    ownerId,
  }).select('-privateKeyEncrypted').exec()
  if (!account.autorenew) {
    return
  }
  const balance = new BN(account.balance)
  const delta = new BN(account.balanceDelta)
  const available = balance.add(delta)
  const subscriptionPrice = new BN(subscriptionOffer.price)
  if (available.lt(subscriptionPrice)){
    return
  }
  const { _doc: accountTransaction } = await AccountTransaction.create({
    accountId: account._id,
    ownerId,
    createdAt: new Date(),
    state: 0,
    blockNumber: 0,
    blockTimestamp: +new Date(), // placeholder for sorting
    type: 'payment',
    amount: subscriptionPrice.toString(),
    transactionId: uuid.v4(),
    from: account.address,
  })
  const [{ nModified }] = await Promise.all([
    Subscription.updateOne({
      _id: subscriptionOffer._id,
    }, {
      active: true,
      accountTransactionId: accountTransaction._id,
    }),
    updateBalanceDelta(ownerId),
  ])
}
