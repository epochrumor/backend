const { auth, authOptional } = require('../middleware/auth')
const mongoose = require('mongoose')
const Note = mongoose.model('Note')
const NoteView = mongoose.model('NoteView')
const NoteRead = mongoose.model('NoteRead')
const User = mongoose.model('User')
const _ = require('lodash')
const { updateReadsForNote } = require('../actions/note')
const { createPendingTransactions } = require('../actions/account')
const error = require('../middleware/catch_error')

module.exports = (app) => {
  // Create many note views
  app.post('/noteviews', authOptional, error(createViews))
}

async function createViews(req, res) {
  const { views } = req.body
  const noteIds = _.chain(views)
    .map('noteId')
    .uniq()
    .map(mongoose.Types.ObjectId)
    .value()
  const notes = await Note.find({
    _id: {
      $in: noteIds,
    }
  }).exec()
  const validNoteIds = _.chain(noteIds)
    .map((id) => id.toString())
    .intersection(
      _.map(notes, (note) => note._id.toString())
    )
    .keyBy((noteId) => noteId)
    .value()
  const now = +new Date()
  const _views = []
  for (const view of views) {
    if (!view.createdAt) {
      res.status(400).json({
        message: 'Missing creation timestamp on at least 1 view',
      })
      return
    }
    if (+new Date(view.createdAt) > now) {
      res.status(400).json({
        message: 'Unable to create view in the future',
      })
      return
    }
    if (!validNoteIds[view.noteId]) continue
    _views.push(view)
  }
  if (_views.length === 0) {
    res.json({ createdCount: 0 })
    return
  }
  const inserted = await NoteView.insertMany(_views.map((view) => ({
    ownerId: req.user._id,
    ...view,
  })))
  res.json({
    createdCount: inserted.length,
  })
  // This executes asynchronously, consider refactoring
  if (!req.user._id) return
  Promise.all(Object.keys(validNoteIds).map(async (noteId) => {
    await updateReadsForNote(noteId, req.user._id)
    const ownerIds = _.chain(notes)
      .map('ownerId')
      .uniq()
      .value()
    await Promise.all(ownerIds.map((ownerId) => createPendingTransactions(ownerId)))
  }))
    .catch((err) => {
      console.log('Error calculating note reads')
      console.log(err)
    })
}
