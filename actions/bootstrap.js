const mongoose = require('mongoose')
const Note = mongoose.model('Note')
const User = mongoose.model('User')
const bcrypt = require('bcryptjs')

module.exports = {
  bootstrap,
}

// Reserve initial notes for admin user
async function bootstrap() {
  const [ userCount, noteCount ] = await Promise.all([
    User.countDocuments({}).exec(),
    Note.countDocuments({}).exec(),
  ])
  if (userCount > 0 || noteCount > 0) return
  // Create admin user
  const salt = await bcrypt.genSalt(10)
  const passwordHash = await bcrypt.hash('default_password', salt)
  try {
    await User.create({
      passwordHash,
      username: 'admin',
      email: 'admin@epochrumor.com',
      createdAt: new Date(),
      updatedAt: new Date(),
      isAdmin: true,
    })
  } catch (err) {
    if (err.code === 11000) {
      // Duplicate key error, ignore
    } else {
      throw err
    }
  }
}
