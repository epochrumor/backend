const Web3 = require('web3')
const test = require('ava')
const { generate } = require('randomstring')
const app = require('..')
const tcpPortUsed = require('tcp-port-used')
const { MIN_DESCRIPTION_LENGTH, MIN_TEXT_LENGTH } = require('../constants')

// For use when testing
// https://expedition.dev/?rpcUrl=http://localhost:7545

module.exports.startServer = async () => {
  const getPort = () => Math.floor(Math.random() * 10000 + 10000)
  let port = getPort()
  while (await tcpPortUsed.check(port)) {
    port = getPort()
  }
  // Wait for db connection to be established
  await app.loadingPromise
  await new Promise((rs) => {
    app.listen(port, rs)
  })
  return { port, shutdown: app.shutdown }
}

module.exports.unwrapAndThrow = (err) => {
  if (!err.response) throw err
  throw {
    status: err.response.status,
    message: err.response.data.message,
  }
}

module.exports.randomstring = (length = 10) => generate(length)

module.exports.userBody = () => {
  return {
    username: generate(10),
    password: generate(10),
    email: `${generate(10)}@email.com`,
  }
}

module.exports.noteBody = () => {
  return {
    description: generate(MIN_DESCRIPTION_LENGTH),
    text: generate(MIN_TEXT_LENGTH),
  }
}

module.exports.deposit = async (accountAddress, weiAmount) => {
  const web3 = new Web3('http://127.0.0.1:7545')
  const wallet = web3.eth.accounts.wallet.add(process.env.TEST_PRIVATE_KEY)
  for (;;) {
    try {
      await web3.eth.sendTransaction({
        from: wallet.index,
        to: accountAddress,
        value: weiAmount,
        gas: 3000000,
      })
      await new Promise(r => setTimeout(r, 2000))
      break
    } catch (err) {
      if (err.message.indexOf('correct nonce') === -1) {
        throw err
      }
      await new Promise(r => setTimeout(r, Math.random() * 1000))
    }
  }
}

// Create a user and pay/subscribe
module.exports.createUser = async (axios) => {
  const { userBody, unwrapAndThrow, deposit } = module.exports
  const { data: user } = await axios.post('/users', {
    ...userBody(),
  }).catch(unwrapAndThrow)
  await new Promise(r => setTimeout(r, 5000))
  const { data: account } = await axios.post('/get/account', {
    token: user.token,
  }).catch(unwrapAndThrow)
  if (!account) {
    throw new Error('Account was not created')
  }
  const { data: subscriptionPrice } = await axios.post('/get/subscription/price', {
    token: user.token,
  }).catch(unwrapAndThrow)
  await deposit(account.address, subscriptionPrice.price)
  // Fucking ganache block timestamps
  // Also wait for the chain daemon
  await new Promise(r => setTimeout(r, 5000))
  const { data: subscription } = await axios.post('/put/subscription', {
    token: user.token,
  }).catch(unwrapAndThrow)
  await new Promise(r => setTimeout(r, 5000))
  return user
}

test('helper stub', (t) => t.pass())
