const mongoose = require('mongoose')

const AccountSchema = new mongoose.Schema(
  {
    ownerId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      index: {
        unique: true,
      },
    },
    createdAt: {
      type: Date,
      required: true,
    },
    address: {
      type: String,
      required: true,
      index: {
        unique: true,
      },
    },
    publicKey: {
      type: String,
      required: true,
    },
    privateKeyEncrypted: {
      type: String,
      required: true,
    },
    balance: {
      type: String,
      required: true,
      default: '0',
    },
    balanceDelta: {
      type: String,
      required: true,
      default: '0',
    },
    balanceEther: {
      type: Number,
      default: 0,
    },
    balanceDeltaEther: {
      type: Number,
      required: true,
      default: 0,
    },
    updatedAt: {
      type: Date,
      required: true,
    },
    transactionCount: {
      type: Number,
      required: true,
      default: 0,
    },
    payoutAddress: {
      type: String,
    },
    autorenew: {
      type: Boolean,
      default: false,
    },
    _locked: {
      type: Boolean,
      default: false,
    },
  }
)

AccountSchema.index({
  address: 1,
})

AccountSchema.index({
  ownerId: 1,
})

mongoose.model('Account', AccountSchema)
