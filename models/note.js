const mongoose = require('mongoose')

const ReactionSchema = new mongoose.Schema(
  {
    reaction: {
      type: String,
      required: true,
    },
    count: {
      type: Number,
      required: true,
    }
  }
)

const NoteSchema = new mongoose.Schema(
  {
    ownerId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    isDraft: {
      type: Boolean,
    },
    isUnmonetized: {
      type: Boolean,
    },
    text: {
      type: String,
      required: true,
    },
    index: {
      type: Number,
      required: true,
    },
    createdAt: {
      type: Date,
      required: true,
    },
    updatedAt: {
      type: Date,
      required: true,
    },
    readCount: {
      type: Number,
      required: true,
      default: 0,
    },
    patchCount: {
      type: Number,
      required: true,
      default: 0,
    },
    reactions: [ReactionSchema],
  }
)

NoteSchema.virtual('owner', {
  ref: 'User',
  localField: 'ownerId',
  foreignField: '_id',
  justOne: true,
})

mongoose.model('Note', NoteSchema)
