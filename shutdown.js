module.exports = {
  addShutdownListener,
  shutdown,
}

process.on('SIGTERM', () => {
  shutdown()
})

process.on('SIGINT', () => {
  shutdown()
})

const listeners = []
function addShutdownListener(l) {
  listeners.push(l)
}

let shuttingDown = false
async function shutdown() {
  if (shuttingDown) return
  shuttingDown = true
  console.log(`Shutting down, waiting for ${listeners.length} daemons`)
  try {
    // execute each listener
    for (const l of listeners) {
      await l()
    }
    // Process should exit automatically when event loop is clear
    // process.exit(0)
  } catch (err) {
    console.log('Unclean shutdown, encountered error')
    console.log(err)
    process.exit(1)
  }
}
