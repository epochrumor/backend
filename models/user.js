const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema(
  {
    username: {
      type: String,
      required: true,
      index: {
        unique: true,
        // https://docs.mongodb.com/manual/reference/collation/
        collation: {
          locale: 'en',
          strength: 2,
        },
      },
    },
    email: {
      type: String,
      required: true,
      index: {
        unique: true,
        collation: {
          locale: 'en',
          strength: 2,
        },
      },
    },
    createdAt: {
      type: Date,
      required: true,
    },
    updatedAt: {
      type: Date,
      required: true,
    },
    passwordHash: {
      type: String,
      required: true,
    },
    followingCount: {
      type: Number,
      required: true,
      default: 0,
    },
    followerCount: {
      type: Number,
      required: true,
      default: 0,
    },
    bio: {
      Type: String,
      default: '',
    },
    isAdmin: {
      type: Boolean,
      default: false,
    },
  }
)

mongoose.model('User', UserSchema)
